<?php $page ='Books';?>
<?php include 'elements/header.php'; ?>

<section class="space--md switchable switchable--switch bg--secondary-2" id="other-donate">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="height-60 imagebg border--round">
                    <div class="background-image-holder"><img alt="background" src="assets/img/hero-img-2.jpg"></div>
                    <div class="pos-vertical-center col-md-6 boxed boxed--lg bg--none">
                        <h1 class="font-600 color--pink">Buy a Book</h1>
                        <p> Welcome to Book Angel. Aside from donating books, Book Angel also have collections on interesting books for sale!</p>
                        <form class="search-bar">
                        	<input class="validate-required" type="text" name="My Input" placeholder="Search a book" />
                        	<button class="btn search-icon" type="submit"><i class="stack-search icon--lg"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="space--sm bg--secondary-2">
	<div class="container mb40">
		<div class="row">
			<div class="col-12">
				<h1 class="font-600">Topics</h1>

			</div>
		</div>
	</div>
	<div class="slider project-slider" data-paging="false" data-arrows="true">
	    <ul class="slides">
	        <li class="col-md-4 col-lg-3 ">
	            <div class="project-thumb hover-element border--round hover--active">
	                <a href="#">
	                    <div class="hover-element__initial">
	                        <div class="background-image-holder" > <img alt="background" src="assets/img/topic-1.svg"> </div>
	                    </div>
	                    <div class="hover-element__reveal">
	                        <div class="project-thumb__title">
	                            <h3 class="font-600 color--white">Mathematics</h3> 
	                        </div>
	                    </div>
	                </a>
	            </div>
	        </li>
	        <li class="col-12 col-md-4 col-lg-3">
	            <div class="project-thumb hover-element border--round hover--active">
	                <a href="#">
	                    <div class="hover-element__initial">
	                        <div class="background-image-holder"> <img alt="background" src="assets/img/topic-2.svg"> </div>
	                    </div>
	                    <div class="hover-element__reveal">
	                        <div class="project-thumb__title">
	                            <h3 class="font-600 color--white">Chemistry</h3>
	                        </div>
	                    </div>
	                </a>
	            </div>
	        </li>
	        <li class="col-12 col-md-4 col-lg-3">
	            <div class="project-thumb hover-element border--round hover--active">
	                <a href="#">
	                    <div class="hover-element__initial">
	                        <div class="background-image-holder"> <img alt="background" src="assets/img/topic-3.svg"> </div>
	                    </div>
	                    <div class="hover-element__reveal">
	                        <div class="project-thumb__title">
	                            <h3 class="font-600 color--white">Physics</h3>
	                        </div>
	                    </div>
	                </a>
	            </div>
	        </li>
	        <li class="col-12 col-md-4 col-lg-3">
	            <div class="project-thumb hover-element border--round hover--active">
	                <a href="#">
	                    <div class="hover-element__initial">
	                        <div class="background-image-holder"> <img alt="background" src="assets/img/topic-4.svg"> </div>
	                    </div>
	                    <div class="hover-element__reveal">
	                        <div class="project-thumb__title">
	                            <h3 class="font-600 color--white">Biology</h3>
	                        </div>
	                    </div>
	                </a>
	            </div>
	        </li>
	        <li class="col-12 col-md-4 col-lg-3">
	            <div class="project-thumb hover-element border--round hover--active">
	                <a href="#">
	                    <div class="hover-element__initial">
	                        <div class="background-image-holder"> <img alt="background" src="assets/img/topic-5.svg"> </div>
	                    </div>
	                    <div class="hover-element__reveal">
	                        <div class="project-thumb__title">
	                            <h3 class="font-600 color--white">Economics</h3>
	                        </div>
	                    </div>
	                </a>
	            </div>
	        </li>
	        
	    </ul>
	</div>
</section>

<section class="space--sm bg--secondary-2 feature-books" >
	<div class="container mb40">
		<div class="row align-items-center mb30">
			<div class="col-6">
				<h1 class="font-600">Our Books</h1>

			</div>
			<div class="col-6 text-right">
				<a href="list-of-books" class="btn btn--blue rounded"><span class="btn__text color--white type--uppercase">View More</a>

			</div>
		</div>
		<div class="row equal-container">
            <div class="col-md-3 col-6 mb20">
                <div class="book feature"> 
                    <div class="pos-relative mb20">
                        <img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-1.png">
                        <label class="label label--primary">Primary 1</label>
                        <!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
                            <span>View More</span>
                        </a> -->
                    </div>

                    <p class="publisher type--fine-print mb00">Rasmed Publication</p>
                    <h4 class="mb--0 font-1 mb00 equal">Civics Education for Primary Schools Bk 2</h4>
                    <p class="author-name">Igwe I. O. et al</p>
                    <a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
                     
                </div>
            </div>
			<div class="col-md-3 col-6 mb20">
                <div class="book feature"> 
                	<div class="pos-relative mb20">
                		<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-2.png">
                		<label class="label label--primary">Primary 1</label>
                		<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
                			<span>View More</span>
                		</a> -->
                		
                	</div>
                    <p class="publisher type--fine-print mb00">Literamed Publication</p>
                    <h4 class="mb--0 font-1 mb00 equal">Brume: The Captain</h4>
                    <p class="author-name">Jane Agunabor</p>
                    	<!-- <h4 class="color--blue mb00">₦ 2000</h4> -->

                    <a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
                    
                    
                </div>
            </div>
           
            <div class="col-md-3 col-6 mb20">
                <div class="book feature"> 
                	<div class="pos-relative mb20">
                		<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-3.png">
                		<label class="label label--primary">Primary 1</label>
                		<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
                			<span>View More</span>
                		</a> -->
                	</div>

                    <p class="publisher type--fine-print mb00">Literamed Publication</p>
                    <h4 class="mb--0 font-1 mb00 equal">Imuli’s Journey</h4>
                    <p class="author-name">Moses Olufemi</p>
                        <!-- <h4 class="color--blue mb00">₦ 2000</h4> -->

                    <a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
                    
                    
                </div>
            </div>
			<div class="col-6 col-md-3 mb20">
				<div class="book feature"> 
					<div class="pos-relative mb20">
                		<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-4.png">
                		<label class="label label--primary">Primary 1</label>
                		<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
                			<span>View More</span>
                		</a> -->
                	</div>
					<p class="publisher type--fine-print mb00">Evans Publication</p>
                    <h4 class="mb--0 font-1 mb00 equal">Modular English Course for Pry Sch Book 2</h4>
                    <p class="author-name">S. O. Ayodele et al</p>
				    
				    <a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
				     
				</div>
			</div>
            <div class="col-md-3 col-6 mb20">

                <div class="book feature">
                	<div class="pos-relative mb20">
                		<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-5.png">
                		<label class="label label--primary">Primary 1</label>
                		<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
                			<span>View More</span>
                		</a> -->
                	</div>
                	<p class="publisher type--fine-print mb00">Evans Publication</p>
                    <h4 class="mb--0 font-1 mb00 equal">Modular Primary Social Studies Book 2</h4>
                    <p class="author-name">Otite O. et al</p>

                    <a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
                    
                </div>
            </div>
            <div class="col-md-3 col-6 mb20">
                <div class="book feature"> 
                	<div class="pos-relative mb20">
                		<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-6.png">
                		<label class="label label--primary">Primary 1</label>
                		<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
                			<span>View More</span>
                		</a> -->
                	</div>
                	<p class="publisher type--fine-print mb00">Oxford Univ Press</p>
                    <h4 class="mb--0 font-1 mb00 equal">My First Oxford Dictionary</h4>
                    <p class="author-name">Evelyn Goldsmith</p>

                    <a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
                     
                </div>
            </div>
            <div class="col-md-3 col-6 mb20">
                <div class="book feature"> 
                	<div class="pos-relative mb20">
                		<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-7.png">
                		<label class="label label--primary">Primary 1</label>
                		<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
                			<span>View More</span>
                		</a> -->
                	</div>
                	<p class="publisher type--fine-print mb00">Nelson</p>
                    <h4 class="mb--0 font-1 mb00 equal">Agricultural Science for Primary Schools Book 2</h4>
                    <p class="author-name">Aiyegbayo J. T. et al</p>

                    <a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
                    
                </div>
            </div>
			<div class="col-6 col-md-3	 mb20">
				<div class="book feature"> 
					<div class="pos-relative mb20">
                		<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-8.png">
                		<label class="label label--primary">Primary 1</label>
                		<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
                			<span>View More</span>
                		</a> -->
                	</div>
					<p class="publisher type--fine-print mb00">Nelson Publication</p>
                    <h4 class="mb--0 font-1 mb00 equal">Basic Science and Technology Book 2</h4>
                    <p class="author-name">Igwe I. O. et al</p>
				    
				    <a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
				     
				</div>
			</div>

        </div>
	</div>
</section>


<?php include 'elements/footer.php'; ?>