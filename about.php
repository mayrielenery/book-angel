<?php $page ='About';?>
<?php include 'elements/header.php'; ?>

 <section class="imagebg height-100 img-height text-center" data-overlay="6">
    <div class="background-image-holder"> <img alt="background" src="assets/img/hero-background-3.jpg"> </div>
    <div class="container pos-vertical-center">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <h4 class="font-600 color--primary mb8 type--uppercase">Trash to Treasure</h4>
                <h1 class=" font-600 mb00">Recycling used books to improve child literacy in Nigeria</h1>
                <a class="btn rounded btn--primary btn--lg inner-link" href="#second">
                	<span class="btn__text type--uppercase">Get Started</span>
                </a>
                
            </div>
            
        </div>

    </div>
    <!-- <img class="pos-absolute image--angel" src="assets/img/img-angel.png"> -->
</section>
<section id="second" class="imagebg bg-dark-blue">
	<div class="container">
		<div class="row mb80 align-items-center justify-content-between">
			<div class="col-md-5">
				<p class="lead mb00">Book angel is a nonprofit social enterprise that aims to balance the deficiency in the education system, one book at a time. Book angel has come about as a result of the realization that the government cannot achieve this goal alone</p>
			</div>
			<div class="col-md-6" data-aos="zoom-in">
				<img src="assets/img/img-1.jpg">
			</div>
		</div>
		<div class="row align-items-center justify-content-between">
			<div class="col-md-6" data-aos="zoom-in">
				<img src="assets/img/img-2.jpg">
			</div>
			<div class="col-md-5">
				<p class="lead mb00">Creating a platform where teachers can through a seamless process request for books that their students need, and philanthropists can provide these books directly for the students. We agree with the adage that says “an investment in education pays the best interest.</p>
			</div>
			
		</div>
	</div>
	
</section>
<section id="projects" class="imagebg height-60 text-center" data-overlay="7">
	<div class="background-image-holder"> <img alt="background" src="assets/img/hero-background-4.jpg"> </div>
	<div class="container pos-vertical-center">
		<div class="row justify-content-center">
			<div class="col-md-7 ">
				 <div class="colorful-border justify-content-center">
                    <span class="border border-xs border-1"></span>
                    <span class="border border-xs border-2"></span>
                    <span class="border border-xs border-3"></span>
                </div> 
                <h4 class="font-600 color--primary mb8 type--uppercase">Our Projects</h4>
				<h1 class="font-600 fs-4">Leave No Child <br> Behind</h1>
			</div>
		</div>
	</div>
</section>
<section class="bg-dark-blue imagebg">
	<div class="container">
		<div class="row mb40">
			<div class="col-md-4" data-aos="zoom-in">
				<div class="project-thumb hover-element border--round hover--active">
                    <a href="#">
                        <div class="hover-element__initial">
                            <div class="background-image-holder" > <img alt="background" src="assets/img/project-1.jpg"> </div>
                        </div>
                        <div class="hover-element__reveal">
                            <div class="project-thumb__title">
                                <h3 class="font-600 ">Nigeria Community Health Project</h3> 
                            </div>
                        </div>
                    </a>
                </div>
			</div>

			<div class="col-md-4" data-aos="zoom-in">
				<div class="project-thumb hover-element border--round hover--active">
				    <a href="#">
				        <div class="hover-element__initial">
				            <div class="background-image-holder"> <img alt="background" src="assets/img/project-2.jpg"> </div>
				        </div>
				        <div class="hover-element__reveal">
				            <div class="project-thumb__title">
				                <h3 class="font-600">Lagos Community Library </h3>
				            </div>
				        </div>
				    </a>
				</div>
			</div>
			<div class="col-md-4" data-aos="zoom-in">
				<div class="project-thumb hover-element border--round hover--active">
                    <a href="#">
                        <div class="hover-element__initial">
                            <div class="background-image-holder"> <img alt="background" src="assets/img/project-3.jpg"> </div>
                        </div>
                        <div class="hover-element__reveal">
                            <div class="project-thumb__title">
                                <h3 class="font-600">Rehoboth Stars Primary School </h3>
                            </div>
                        </div>
                    </a>
                </div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4" data-aos="zoom-in">
				<div class="project-thumb hover-element border--round hover--active">
                    <a href="#">
                        <div class="hover-element__initial">
                            <div class="background-image-holder"> <img alt="background" src="assets/img/project-4.jpg"> </div>
                        </div>
                        <div class="hover-element__reveal">
                            <div class="project-thumb__title">
                                <h3 class="font-600">St. Stephen WAEC Primary School. Lagos Island </h3>
                            </div>
                        </div>
                    </a>
                </div>
			</div>
			<div class="col-md-4" data-aos="zoom-in">
				<div class="project-thumb hover-element border--round hover--active">
                    <a href="#">
                        <div class="hover-element__initial">
                            <div class="background-image-holder"> <img alt="background" src="assets/img/project-5.jpg"> </div>
                        </div>
                        <div class="hover-element__reveal">
                            <div class="project-thumb__title">
                                <h3 class="font-600">The Children’s Voice Book Drive</h3>
                            </div>
                        </div>
                    </a>
                </div>
			</div>
		</div>
	</div>
</section>
<?php include 'elements/footer.php'; ?>