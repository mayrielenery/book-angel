<?php $page ='Books';?>
<?php include 'elements/header.php'; ?>



<section class="space--md bg--secondary" id="search-area">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-12">
				<div class="search-box-1">
					<form >
						<div class="search-bar search-container-bar">
							<div class="toolbar-search">
								<div class="d-flex">
									<svg xmlns="http://www.w3.org/2000/svg" class="search-panel-query-icon" width="24px" height="24px" viewBox="0 0 24 24"> <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path> </svg>
									<input class="validate-required" type="text" name="My Input" placeholder="Search a book" />
									<div class="spacer"></div>
								</div>
							</div>
							<div class="toolbar-text">
								<div class="cat-dropdown">
									<!-- Example single dropdown button -->
									<div class="input-group-append">
									    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Year</button>
									    <div class="dropdown-menu">
									      <a class="dropdown-item" href="#">2020</a>
									      <a class="dropdown-item" href="#">2019</a>
									      <a class="dropdown-item" href="#">2018</a>
									      <a class="dropdown-item" href="#">2018</a>
									      
									    </div>
									</div>
								</div>

								<div class="input-text">
									<input type="text" name="" placeholder="Search a publisher">
								</div>
								<div class="spacer"></div>
								<div class="cat-dropdown second">
									<!-- Example single dropdown button -->
									<div class="input-group-append">
									    <button class="btn dropdown-toggle type--uppercase" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Subject</button>
									    <div class="dropdown-menu">
									      <a class="dropdown-item" href="#">Mathematics</a>
									      <a class="dropdown-item" href="#">Science</a>
									      <a class="dropdown-item" href="#">Economics</a>
									      <a class="dropdown-item" href="#">English</a>
									      
									    </div>
									</div>
								</div>
								<div class="cat-dropdown third">
									<!-- Example single dropdown button -->
									<div class="input-group-append">
									    <button class="btn dropdown-toggle type--uppercase" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Type of School</button>
									    <div class="dropdown-menu">
									      <a class="dropdown-item" href="#">Primary 1</a>
									      <a class="dropdown-item" href="#">Primary 2</a>
									      <a class="dropdown-item" href="#">Primary 3</a>
									      <a class="dropdown-item" href="#">Primary 4</a>
									      <a class="dropdown-item" href="#">Primary 5</a>
									    </div>
									</div>
								</div>
							</div>
						</div>
						<!-- <div id="category-dropdown" class="input-group-append d-inline-block">
						    <button class="btn dropdown-toggle rounded" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Type of Government</button>
						    <div class="dropdown-menu">
						      <a class="dropdown-item" href="#">Private</a>
						      <a class="dropdown-item" href="#">Missionary</a>
						      <a class="dropdown-item" href="#">Owned</a>
						      
						      
						    </div>
						</div> -->
						
					</form>
					
				</div>
			</div>
		</div>
	</div>
</section>
<section class="list-of-book bg--secondary">
	<div class="container">
		<div class="row justify-content-center equal-container">
			<div class="col-12 text-center mb30">
				<h1>List of books</h1>
			</div>
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="primary1-tab" data-toggle="tab" href="#primary1" role="tab" aria-controls="primary1" aria-selected="true">Primary1</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="primary2-tab" data-toggle="tab" href="#primary2" role="tab" aria-controls="primary2" aria-selected="false">Primary2</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="primary3-tab" data-toggle="tab" href="#primary3" role="tab" aria-controls="primary3" aria-selected="false">Primary3</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="primary4-tab" data-toggle="tab" href="#primary4" role="tab" aria-controls="primary5" aria-selected="false">Primary4</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="primary5-tab" data-toggle="tab" href="#primary5" role="tab" aria-controls="primary5" aria-selected="false">Primary5</a>
				</li>
			</ul>
		</div>
		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane active" id="primary1" role="tabpanel" aria-labelledby="primary1-tab">
				<section>
					<div  class="container">
						<div class="row">
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-1.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Rasmed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Civics Education for Primary Schools Bk 2</h4>
									<p class="author-name">Adesola Luqman et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-2.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
										
									</div>
									<p class="publisher type--fine-print mb00">Literamed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Brume: The Captain</h4>
									<p class="author-name">Jane Agunabor</p>
										<!-- <h4 class="color--blue mb00">₦ 2000</h4> -->

									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-3.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Literamed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Imuli’s Journey</h4>
									<p class="author-name">Moses Olufemi</p>
										<!-- <h4 class="color--blue mb00">₦ 2000</h4> -->

									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-4.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Modular English Course for Pry Sch Book 2</h4>
									<p class="author-name">S. O. Ayodele et al</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature">
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-5.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Modular Primary Social Studies Book 2</h4>
									<p class="author-name">Otite O. et al</p>

									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-6.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Oxford Univ Press</p>
									<h4 class="mb--0 font-1 mb00 equal">My First Oxford Dictionary</h4>
									<p class="author-name">Evelyn Goldsmith</p>

									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-7.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Nelson</p>
									<h4 class="mb--0 font-1 mb00 equal">Agricultural Science for Primary Schools Book 2</h4>
									<p class="author-name">Aiyegbayo J. T. et al</p>

									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3	 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-8.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Nelson Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Basic Science and Technology Book 2</h4>
									<p class="author-name">Igwe I. O. et al</p>
									
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature">
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-9.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Primary Health Education Book 2</h4>
									<p class="author-name">Ori-Aifo et al</p>

									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-10.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Bounty Press Ltd.</p>
									<h4 class="mb--0 font-1 mb00 equal">Spelling Book 0</h4>
									<p class="author-name">John Smith</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-11.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Structural Eng. Workbooks Book 2</p>
									<h4 class="mb--0 font-1 mb00 equal">Imuli’s Journey</h4>
									<p class="author-name">Ronald Ridouti</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-12.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Spectrum Books Ltd</p>
									<h4 class="mb--0 font-1 mb00 equal">Imuli’s JourneThe Queen Primer – Part 2</h4>
									<p class="author-name">Victoria Regina</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
									
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-1-13.png">
										<label class="label label--primary">Primary 1</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Rasmed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">New Approach to Verbal Reasoning Bk. 2</h4>
									<p class="author-name">T.A.O. Olayiwola</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<!-- Primary 2 -->
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-1.png">
										<label class="label label--primary">Primary 2</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Rasmed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Basic Creative and Cultural Arts Book 2</h4>
									<p class="author-name">Kolade Salami</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="tab-pane" id="primary2" role="tabpanel" aria-labelledby="primary2-tab">
				<section>
					<div  class="container">
						<div class="row">
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-5.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Evans</p>
									<h4 class="mb--0 font-1 mb00 equal">Modular English Course for Pry Sch Book 3</h4>
									<p class="author-name">S. O. Ayodele et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-3.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Longman</p>
									<h4 class="mb--0 font-1 mb00 equal">Brighter Grammar Book 1</h4>
									<p class="author-name">Phebean Ogundipe et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-9.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Bounty Press Ltd.</p>
									<h4 class="mb--0 font-1 mb00 equal">Spelling Book 1</h4>
									<p class="author-name">John Smith</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-7.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Oxford Univ Press</p>
									<h4 class="mb--0 font-1 mb00 equal">My First Oxford Dictionary</h4>
									<p class="author-name">Evelyn Goldsmith</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-11.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Spectrum Books Ltd</p>
									<h4 class="mb--0 font-1 mb00 equal">The Queen Primer – Part 2</h4>
									<p class="author-name">Victoria Regina</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-10.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Literamed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">The Clique and the Notorious Three</h4>
									<p class="author-name">Jane Agunabor</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-12.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Literamed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">The Singing Nights Birds & The King’s Daughter</h4>
									<p class="author-name">Olajire Olanlokun</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-13.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Rasmed</p>
									<h4 class="mb--0 font-1 mb00 equal">New Approach to Verbal Reasoning Bk. 3</h4>
									<p class="author-name">T.A.O. Olayiwola</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-2.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Nelson</p>
									<h4 class="mb--0 font-1 mb00 equal">Basic Science and Technology Book 3</h4>
									<p class="author-name">Igwe I. O. et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-8.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Evans</p>
									<h4 class="mb--0 font-1 mb00 equal">Primary Health Education Book 3</h4>
									<p class="author-name">Ori-Aifo et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-6.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Evans</p>
									<h4 class="mb--0 font-1 mb00 equal">Modular Primary Social Studies Book 3</h4>
									<p class="author-name">Otite O. et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-4.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Rasmed</p>
									<h4 class="mb--0 font-1 mb00 equal">Civics Education for Primary Schools Bk 3</h4>
									<p class="author-name">Adesola Luqman et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-1-2/primary-2-1.png">
										<label class="label label--primary">Primary 2</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>

									<p class="publisher type--fine-print mb00">Rasmed</p>
									<h4 class="mb--0 font-1 mb00 equal">Basic Creative and Cultural Arts Book 2</h4>
									<p class="author-name">Kolade Salami</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="tab-pane" id="primary3" role="tabpanel" aria-labelledby="primary3-tab">
				<section>
					<div  class="container">
						<div class="row">
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-1.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans</p>
									<h4 class="mb--0 font-1 mb00 equal">Modular English Course for Pry Sch Book 4</h4>
									<p class="author-name">S.O. Ayodele et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-2.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Longman</p>
									<h4 class="mb--0 font-1 mb00 equal">Brighter Grammar Book 2</h4>
									<p class="author-name">Phebean Ogundipe et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-3.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Bounty Press Ltd.</p>
									<h4 class="mb--0 font-1 mb00 equal">Spelling Book 2</h4>
									<p class="author-name">John Smith</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-4.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Longman</p>
									<h4 class="mb--0 font-1 mb00 equal">The New Method English Dictionary</h4>
									<p class="author-name">Michael West	</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-5.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Spectrum Books Ltd.</p>
									<h4 class="mb--0 font-1 mb00 equal">The Queen Primer – Part 2</h4>
									<p class="author-name">Victoria Regina</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-6.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Literamed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Waiting for the Messiah</h4>
									<p class="author-name">Ifeanyi ifoegbuna</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-7.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Literamed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">God Answers Okey’s Prayer & Great Fight</h4>
									<p class="author-name">Chudi Ebiringa</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-8.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Literamed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">The Legend: Chief Obafemi Awolowo</h4>
									<p class="author-name">Olajire Olanlokun</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-9.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Rasmed</p>
									<h4 class="mb--0 font-1 mb00 equal">New Approach to Verbal Reasoning Bk. 4</h4>
									<p class="author-name">T.A.O. Olayiwola</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-10.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Nelson</p>
									<h4 class="mb--0 font-1 mb00 equal">Basic Science and Technology Book 4</h4>
									<p class="author-name">Igewe I. O. et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-11.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans</p>
									<h4 class="mb--0 font-1 mb00 equal">Primary Health Education Book 4</h4>
									<p class="author-name">Ori-Aifo et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-12.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Nelson</p>
									<h4 class="mb--0 font-1 mb00 equal">Agricultural Science for Primary Schools Book 4</h4>
									<p class="author-name">Aiyegbayo J.T. et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-13.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans</p>
									<h4 class="mb--0 font-1 mb00 equal">Modular Primary Social Studies Book 4</h4>
									<p class="author-name">Otite O. et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-14.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Rasmed</p>
									<h4 class="mb--0 font-1 mb00 equal">Civics Education for Primary Schools Bk 4</h4>
									<p class="author-name">Adesola Luqman et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-15.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Rasmed</p>
									<h4 class="mb--0 font-1 mb00 equal">Basic Creative and Cultural Arts Book 4</h4>
									<p class="author-name">Kolade Salami</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-16.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Rasmed</p>
									<h4 class="mb--0 font-1 mb00 equal">New Approach to Quantitative Reasoning Bk. 4</h4>
									<p class="author-name">T.A.O. Olayiwola</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-17.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">U.P. Plc</p>
									<h4 class="mb--0 font-1 mb00 equal">Rope Of Allah Book 4</h4>
									<p class="author-name">S.H.A. Malik et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-3/primary-3-18.png">
										<label class="label label--primary">Primary 3</label>
									<!--  <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Longman</p>
									<h4 class="mb--0 font-1 mb00 equal">Alawiye Iwe Kerin</h4>
									<p class="author-name">J.F. Odunjo</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="tab-pane" id="primary4" role="tabpanel" aria-labelledby="primary4-tab">
				<section>
					<div  class="container">
						<div class="row">
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-1.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans</p>
									<h4 class="mb--0 font-1 mb00 equal">Modular English Course for Pry Sch Book 5</h4>
									<p class="author-name">S.O. Ayodele et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-2.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Longman</p>
									<h4 class="mb--0 font-1 mb00 equal">Brighter Grammar Book 3</h4>
									<p class="author-name">Phebean Ogundipe et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-3.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Bounty Press Ltd.</p>
									<h4 class="mb--0 font-1 mb00 equal">Spelling Book 3</h4>
									<p class="author-name">John Smith</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-4.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Longman</p>
									<h4 class="mb--0 font-1 mb00 equal">The New Method English Dictionary</h4>
									<p class="author-name">Michael West</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-5.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Extension Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Dayo’s Fortune</h4>
									<p class="author-name">Samon O. Olatunji</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-6.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Literamed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Adamu and His Horse</h4>
									<p class="author-name">Dapo Adeleke</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-7.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Rasmed</p>
									<h4 class="mb--0 font-1 mb00 equal">New Approach to Verbal Reasoning Bk. 5</h4>
									<p class="author-name">T.A.O Olayiwola</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-8.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Nelson</p>
									<h4 class="mb--0 font-1 mb00 equal">Basic Science and Technology Book 5</h4>
									<p class="author-name">Igwe I.O. et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-9.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans</p>
									<h4 class="mb--0 font-1 mb00 equal">Primary Health Education Book 5</h4>
									<p class="author-name">Ori-Aifo et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-10.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Nelson</p>
									<h4 class="mb--0 font-1 mb00 equal">Agricultural Science for Primary Schools Book 5</h4>
									<p class="author-name">Aiyegbayo J.T. et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-11.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans</p>
									<h4 class="mb--0 font-1 mb00 equal">Modular Primary Social Studies Book 5</h4>
									<p class="author-name">Otite O. et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-12.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Rasmed</p>
									<h4 class="mb--0 font-1 mb00 equal">Civics Education for Primary Schools Bk 5</h4>
									<p class="author-name">Adesola Luqmen et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-13.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Literamed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Comprehensive Home Economics for Pry Sch 5</h4>
									<p class="author-name">Ogunjinmi, et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-14.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Rasmed</p>
									<h4 class="mb--0 font-1 mb00 equal">Basic Creative and Cultural Arts Book 5</h4>
									<p class="author-name">Kolade Salami</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-15.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">U.P. Plc</p>
									<h4 class="mb--0 font-1 mb00 equal">Rope Of Allah Book 5</h4>
									<p class="author-name">S.H.A. Malik et al</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-16.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Longman</p>
									<h4 class="mb--0 font-1 mb00 equal">Alawiye Iwe Karun</h4>
									<p class="author-name">J.F. Odunjo</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-17.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00"></p>
									<h4 class="mb--0 font-1 mb00 equal">Nationwide Common Entrance Examinations on Maths & Quantitative Aptitude Tests</h4>
									<p class="author-name">Ugo C. Ugo</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-18.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00"></p>
									<h4 class="mb--0 font-1 mb00 equal">Nationwide Common Entrance Examinations on English Lang. &Verbal Aptitude Tests</h4>
									<p class="author-name">Ugo C. Ugo</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-md-3 col-6 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-4/primary-4-19.png">
										<label class="label label--primary">Primary 4</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00"></p>
									<h4 class="mb--0 font-1 mb00 equal">Nationwide Vocational Aptitude Tests (General Studies, Social Studies, Current Affairs)</h4>
									<p class="author-name">Ugo C. Ugo</p>
									<a href="#"class="btn btn--sm btn--bordered-blue block rounded "><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="tab-pane" id="primary5" role="tabpanel" aria-labelledby="primary5-tab">
				<section>
					<div  class="container">
						<div class="row">
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-5/primary-5-1.png">
										<label class="label label--primary">Primary 5</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Modular English Course for Pry Sch Book 6</h4>
									<p class="author-name">S. O. Ayodele et al</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-5/primary-5-2.png">
										<label class="label label--primary">Primary 5</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Nelson</p>
									<h4 class="mb--0 font-1 mb00 equal">Basic Science and Technology Book 6</h4>
									<p class="author-name">Igwe I.O. et al</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-5/primary-5-3.png">
										<label class="label label--primary">Primary 5</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Primary Health Education Book 6</h4>
									<p class="author-name">Ori-Aifo et al</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-5/primary-5-4.png">
										<label class="label label--primary">Primary 5</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Nelson Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Agricultural Science for Primary Schools Book 6</h4>
									<p class="author-name">Aiyegbayo J.T. et al</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-5/primary-5-5.png">
										<label class="label label--primary">Primary 5</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Evans Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Modular Primary Social Studies Book 6</h4>
									<p class="author-name">Otite O. et al</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-5/primary-5-6.png">
										<label class="label label--primary">Primary 5</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Rasmed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Civics Education for Primary Schools Bk 6</h4>
									<p class="author-name">Adesola Luqman et al.</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-5/primary-5-7.png">
										<label class="label label--primary">Primary 5</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Rasmed Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Basic Creative and Cultural Arts Book 6</h4>
									<p class="author-name">Kolade Salami</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-5/primary-5-8.png">
										<label class="label label--primary">Primary 5</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00">Longman Publication</p>
									<h4 class="mb--0 font-1 mb00 equal">Alawiye Iwe Kefa</h4>
									<p class="author-name">J.F. Odunjo</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-5/primary-5-9.png">
										<label class="label label--primary">Primary 5</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00"></p>
									<h4 class="mb--0 font-1 mb00 equal">Nationwide Common Entrance Examinations on Maths & Quantitative Aptitude Tests</h4>
									<p class="author-name">Ugo C. Ugo</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-5/primary-5-10.png">
										<label class="label label--primary">Primary 5</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00"></p>
									<h4 class="mb--0 font-1 mb00 equal">Nationwide Common Entrance Examinations on English Lang. &Verbal Aptitude Tests</h4>
									<p class="author-name">Ugo C. Ugo</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
							<div class="col-6 col-md-3 mb20">
								<div class="book feature"> 
									<div class="pos-relative mb20">
										<img alt="Pic" class="mb00" src="assets/img/book-1/primary-5/primary-5-11.png">
										<label class="label label--primary">Primary 5</label>
										<!-- <a href="#" class="justify-content-center align-items-center py-2 text-uppercase color--black border-bottom seemore">
											<span>View More</span>
										</a> -->
									</div>
									<p class="publisher type--fine-print mb00"></p>
									<h4 class="mb--0 font-1 mb00 equal">Nationwide Vocational Aptitude Tests (General Studies, Social Studies, Current Affairs)</h4>
									<p class="author-name">Ugo C. Ugo</p>
									
									<a hrefbtn--sm ="#"class="btn btn--bordered-blue block rounded"><span class="btn__text type--uppercase">Add to cart</span></a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>

<?php include 'elements/footer.php'; ?>