<?php $page ='Home';?>
<?php include 'elements/header.php'; ?>
        
        
    <section class="cover cover-features height-xs-100 imagebg bg-dark-blue space--lg" id="home-banner">
        <svg class="svg-rect" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" opacity="0.317" width="606.948" height="510.216" viewBox="0 0 606.948 510.216"><defs><linearGradient id="a" x1="-2.931" y1="0.084" x2="1.537" y2="1.231" gradientUnits="objectBoundingBox"><stop offset="0" stop-color="#a037fa"/><stop offset="1" stop-color="#e3ecff"/></linearGradient></defs><g transform="translate(-1056 42.108)" opacity="0.271"><rect width="48" height="617" transform="matrix(0.574, 0.819, -0.819, 0.574, 1561.417, -42.108)" opacity="0.617" fill="url(#a)"/><rect width="48" height="617" transform="matrix(0.574, 0.819, -0.819, 0.574, 1599.417, 13.892)" opacity="0.617" fill="url(#a)"/><rect width="48" height="617" transform="matrix(0.574, 0.819, -0.819, 0.574, 1635.417, 74.892)" opacity="0.617" fill="url(#a)"/></g></svg>
        <div class="container pos-vertical-center-xs">
            <div class="row">
                <div class="col-md-9 col-lg-9" >
                    <h1 class=" title font-600 animated fadeInUp"> Help Improve literacy and Numeracy in Nigeria by donating Books.</h1>
                    
                </div>
            </div>
            <div class="row boxes equal-container sm">
                
                <!-- <div class="col-12 col-sm-4 col-lg-4 hvr-pulse">
                    <div class="feature feature-1 boxed box-1 bg--yellow boxed--border mb-sm-24" data-aos="zoom-in" data-aos-delay="200" >
                        <svg class="pos-absolute first" xmlns="http://www.w3.org/2000/svg" width="334.784" height="314.604" viewBox="0 0 334.784 314.604"><g transform="matrix(0.809, -0.588, 0.588, 0.809, 0, 159.878)" opacity="0.132" style="mix-blend-mode:multiply;isolation:isolate"><path class="cart" d="M87.636,159.378h.012l.031,0H232.158a7.97,7.97,0,0,0,7.662-5.779L271.694,42.033a7.968,7.968,0,0,0-7.662-10.158H69.258L63.562,6.24A7.97,7.97,0,0,0,55.782,0H7.969a7.969,7.969,0,0,0,0,15.938H49.39C50.4,20.48,76.65,138.613,78.161,145.41a23.9,23.9,0,0,0,9.5,45.841h144.5a7.969,7.969,0,0,0,0-15.938H87.657a7.968,7.968,0,0,1-.021-15.936ZM253.468,47.813l-27.322,95.626H94.048L72.8,47.813Zm0,0"/><path class="cart" d="M150,383.906A23.906,23.906,0,1,0,173.906,360,23.934,23.934,0,0,0,150,383.906Zm23.906-7.969a7.969,7.969,0,1,1-7.969,7.969A7.979,7.979,0,0,1,173.906,375.938Zm0,0" transform="translate(-70.312 -168.749)"/><path class="cart" d="M362,383.906A23.906,23.906,0,1,0,385.906,360,23.934,23.934,0,0,0,362,383.906Zm23.906-7.969a7.969,7.969,0,1,1-7.969,7.969A7.979,7.979,0,0,1,385.906,375.938Zm0,0" transform="translate(-169.686 -168.749)"/></g></svg>
                        <hr class="short blue">
                        <h1 class="lh-1 title font-600 color--blue equal">Buy a book</h1>
                        

                        <a href="#" class="btn btn--blue rounded">
                            <span class="btn__text type--uppercase">Buy Now</span>
                        </a> 
                    </div>
                </div> -->
            
                <div class="col-12 col-sm-4 col-lg-6 hvr-shrink">
                    <div class="feature feature-1 boxed box-1 bg--lightgreen boxed--border mb-sm-24" data-aos="zoom-in" data-aos-delay="200" >
                        <svg class="pos-absolute" xmlns="http://www.w3.org/2000/svg" width="313" height="263.791" viewBox="0 0 313 263.791"><g opacity="0.13" style="mix-blend-mode:multiply;isolation:isolate;"><path d="M275.8,129c-6.225,5.254-12.661,10.685-19.5,16.611a8.437,8.437,0,0,1-11.052,0c-6.838-5.926-13.274-11.358-19.5-16.611-11.316-9.552-21.089-17.8-29.461-25.8-15.275-14.59-29.41-31.208-29.41-53.809C166.883,22.425,186.1,0,213.05,0c16.047,0,28.87,8.314,37.729,21.694C259.641,8.314,272.462,0,288.508,0c27,0,46.169,22.48,46.169,49.395,0,14.164-5.478,27.211-17.24,41.063C307.251,102.457,292.679,114.757,275.8,129Zm12.7-112.122c-8.879,0-16.447,4.19-22.491,12.456a50.437,50.437,0,0,0-7.106,14.056,8.432,8.432,0,0,1-16.255.029,50.279,50.279,0,0,0-7.115-14.082c-6.043-8.266-13.61-12.459-22.491-12.459-16.7,0-29.29,13.979-29.29,32.517,0,13.311,6.783,24.976,24.19,41.6,8,7.638,17.587,15.732,28.69,25.1,4.57,3.858,9.255,7.811,14.139,11.995,4.887-4.184,9.569-8.136,14.139-11.993C297.9,88.269,317.8,71.468,317.8,49.395,317.8,30.856,305.208,16.877,288.508,16.877Zm0,0" transform="translate(-140.548)"/><path d="M3.965,295.881c7.735-11.863,24.155-16,37.375-9.413l53.19,26.471c5.984-16.9,23.273-29.151,43.612-29.151H172.66L207.408,267.8a53.693,53.693,0,0,1,52,3.973c8.188,5.48,20.964,12.012,35.335,12.012h9.091c5.065,0,9.17,3.779,9.17,8.438v67.509c0,4.66-4.1,8.438-9.17,8.438H230.609l-55.2,8.459a188.815,188.815,0,0,1-28.637,2.18,185.627,185.627,0,0,1-91.5-23.893L13.278,331.1C.29,323.722-3.889,307.924,3.965,295.881Zm290.693,4.783a72.566,72.566,0,0,1-18.954-2.639v53.268h18.954ZM22.907,316.735l41.986,23.818a167.2,167.2,0,0,0,107.5,19.43c60.431-9.262,56.371-8.69,57.46-8.69h27.512V290.669q-4.42-2.357-8.747-5.247a34.084,34.084,0,0,0-33-2.527l-36.684,16.877a9.81,9.81,0,0,1-4.1.892H138.142c-11.957,0-22.156,7.053-25.941,16.875h62.625c5.065,0,9.17,3.779,9.17,8.44s-4.1,8.325-9.17,8.325h-73.2a9.688,9.688,0,0,1-4.683-.978L32.611,301.31a10.145,10.145,0,0,0-12.919,3.252A8.547,8.547,0,0,0,22.907,316.735Zm0,0" transform="translate(0 -115.019)"/></g></svg>
                        
                        <hr class="short">
                        <h1 class="lh-1 title font-600 equal">Donate a <br> book</h1>
                        

                        <a href="donate-book" class="btn btn--red rounded">
                            <span class="btn__text type--uppercase ">Donate Now</span>
                        </a>
                    </div>
                </div>
            
                <div class="col-12 col-sm-4 col-lg-6 hvr-shrink">
                    <div class="feature feature-1 boxed box-1 bg--blue boxed--border mb-sm-24" data-aos="zoom-in" data-aos-delay="500" >
                        <svg class="pos-absolute" xmlns="http://www.w3.org/2000/svg" width="274" height="266.708" viewBox="0 0 274 266.708"><g transform="translate(0 -6.786)" opacity="0.13" style="mix-blend-mode:multiply;isolation:isolate;"><path d="M271.051,114.988l-37.087-30.6V51.92a8.058,8.058,0,0,0-8.059-8.059H184.837L142.131,8.628a8.069,8.069,0,0,0-10.262,0L89.163,43.861H48.1a8.058,8.058,0,0,0-8.059,8.059V84.392L2.95,114.988l-.021.021A8.129,8.129,0,0,0,0,121.237v144.2a8.057,8.057,0,0,0,8.059,8.059H265.941A8.051,8.051,0,0,0,274,265.436v-144.2a8.311,8.311,0,0,0-2.95-6.248ZM257.882,246.755,188.92,181.678c42.093-27.758,30.919-20.4,68.962-45.479ZM21.6,120.506c11.066-9.129,7.037-5.8,18.439-15.215v27.373C29.906,125.977,25.607,123.152,21.6,120.506ZM56.154,59.979H217.846V143.3c-72.32,47.7-26.283,17.328-80.846,53.317C98.167,171,115.713,182.579,56.154,143.3V59.979ZM137,25.294l22.506,18.568H114.494Zm115.4,95.212c-3.88,2.561-8.635,5.687-18.439,12.158V105.291c11.419,9.426,7.365,6.081,18.439,15.215ZM16.118,136.2,85.08,181.678,16.118,246.755Zm82.78,54.6,33.664,22.2a8.083,8.083,0,0,0,8.875,0l33.664-22.2,70.558,66.582H28.34Z"/><path d="M219.379,173.648a8.062,8.062,0,0,0,8.059-8.059,7.34,7.34,0,0,1,7.532-7.35,7.424,7.424,0,0,1,7.167,7.167c.116,4.9-3.215,5.558-7.291,10.385a33.845,33.845,0,0,0-8.118,22.183,8.061,8.061,0,1,0,16.118-.253c0-5.012,2.1-9.933,6.8-13.974a23.467,23.467,0,1,0-38.328-18.159A8.062,8.062,0,0,0,219.379,173.648Z" transform="translate(-97.787 -62.625)"/><path d="M248.059,268.434A8.058,8.058,0,0,0,240,276.493v4.717a8.059,8.059,0,1,0,16.118,0v-4.717A8.058,8.058,0,0,0,248.059,268.434Z" transform="translate(-111.059 -121.076)"/></g></svg>
                        
                        <hr class="short">
                        <h1 class="lh-1 title font-600 equal">Request for<br> a book</h1>
                        

                        <a href="request-book" class="btn btn--pink rounded">
                            <span class="btn__text type--uppercase">Request Now</span>
                        </a> 
                        
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <section class="imagebg height-100" data-overlay="6">
        <div class="background-image-holder"> <img alt="background" src="assets/img/hero-background-2.jpg"> </div>
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-md-7">
                    <div class="colorful-border">
                        <span class="border border-1"></span>
                        <span class="border border-2"></span>
                        <span class="border border-3"></span>
                    </div>
                    <h3 class="font-600">Book angel is a nonprofit social enterprise that aims to balance the deficiency in the education system, one book at a time. </h3>
                    
                </div>
                
            </div>

        </div>
        <!-- <img class="pos-absolute image--angel" src="assets/img/img-angel.png"> -->
    </section>
    <section class=" imagebg bg-dark-blue">
        <div class="background-image-holder"> <img alt="background" src="img/hero-1.jpg"> </div>
        <div class="container">
            <div class="row mb80">
                <div class="col-12">
                    <div class="colorful-border">
                        <span class="border border-xs border-1"></span>
                        <span class="border border-xs border-2"></span>
                        <span class="border border-xs border-3"></span>
                    </div> 
                    <h1 class="font-600">Projects that gives impact</h1> 
                </div>

                <div class="col ">
                    
                </div>
            </div>

            
        </div>
        <div class="slider project-slider" data-paging="false" data-arrows="true">
            <ul class="slides">
                <li class="col-md-4 col-lg-3 ">
                    <div class="project-thumb hover-element border--round hover--active">
                        <a href="#">
                            <div class="hover-element__initial">
                                <div class="background-image-holder" > <img alt="background" src="assets/img/project-1.jpg"> </div>
                            </div>
                            <div class="hover-element__reveal">
                                <div class="project-thumb__title">
                                    <h3 class="font-600 ">Nigeria Community Health Project</h3> 
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <li class="col-12 col-md-4 col-lg-3">
                    <div class="project-thumb hover-element border--round hover--active">
                        <a href="#">
                            <div class="hover-element__initial">
                                <div class="background-image-holder"> <img alt="background" src="assets/img/project-2.jpg"> </div>
                            </div>
                            <div class="hover-element__reveal">
                                <div class="project-thumb__title">
                                    <h3 class="font-600">Lagos Community Library </h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <li class="col-12 col-md-4 col-lg-3">
                    <div class="project-thumb hover-element border--round hover--active">
                        <a href="#">
                            <div class="hover-element__initial">
                                <div class="background-image-holder"> <img alt="background" src="assets/img/project-3.jpg"> </div>
                            </div>
                            <div class="hover-element__reveal">
                                <div class="project-thumb__title">
                                    <h3 class="font-600">Rehoboth Stars Nursery/Primary School </h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <li class="col-12 col-md-4 col-lg-3">
                    <div class="project-thumb hover-element border--round hover--active">
                        <a href="#">
                            <div class="hover-element__initial">
                                <div class="background-image-holder"> <img alt="background" src="assets/img/project-4.jpg"> </div>
                            </div>
                            <div class="hover-element__reveal">
                                <div class="project-thumb__title">
                                    <h3 class="font-600">St. Stephen WAEC Primary School. Lagos Island </h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <li class="col-12 col-md-4 col-lg-3">
                    <div class="project-thumb hover-element border--round hover--active">
                        <a href="#">
                            <div class="hover-element__initial">
                                <div class="background-image-holder"> <img alt="background" src="assets/img/project-5.jpg"> </div>
                            </div>
                            <div class="hover-element__reveal">
                                <div class="project-thumb__title">
                                    <h3 class="font-600">The Children’s Voice Book Drive</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                
            </ul>
        </div>

        <!-- <div class="progressbar"></div> -->
    </section>
        
            
<?php include 'elements/footer.php'; ?>
            