<?php $page ='Donate';?>
<?php include 'elements/header.php'; ?>


<section class="cover cover-features height-xs-100 bg--secondary-2 space--lg" id="donate-banner">
       
        <div class="container pos-vertical-center-xs">
            <div class="row mb8">
                <div class="col-md-9 col-lg-7" >
                    <h1 class=" title font-600 animated fadeInUp"> We leave no Child Behind. Donate at Book Angel!</h1>
                    <p>You can donate used books or buy books for Angel Book! <br>We made it easy for you. </p>
                    <a href="#to-steps" class="btn btn--blue rounded inner-link ">
						<span class="btn__text type--uppercase">View Process</span>
					</a>
                </div>
            </div>
            <div class="row boxes equal-container sm">
            
                <div class="col-12 col-sm-4 col-lg-6 hvr-grow">
                	<div class="modal-instance">
	                    <a href="#" class="modal-trigger">
	                    	<div class="feature feature-1 boxed box-1 bg--lightpink boxed--border mb-sm-24" data-aos="zoom-in" data-aos-delay="200" >
	                    		<h5 class="type--uppercase color--blue mb16">Click here to</h5>
	                    	    <h1 class=" title font-600 equal color--white">Donate <br> Used Book</h1>
	                    	    
	                    	    <svg xmlns="http://www.w3.org/2000/svg" class="pos-absolute circle-yellow" width="262" height="262" viewBox="0 0 262 262"><circle cx="131" cy="131" r="131" fill="#ecff86"/></svg>
	                    	    <img src="assets/img/book-1.svg" class="anim-up-down-2 pos-absolute book-1">
	                    	</div>
	                    </a>
						<div class="modal-container">
	                        <div class="modal-content md">
	                            <div class="boxed boxed--lg text-center">
	                               
		                                <form class="justify-content-center">
		                                	<div class="col-md-12 col-lg-12">
		                                		<h2 class="mb8">Donate a Book</h2>
		                                	</div>
		                                    
	                                    	<div class="col-md-12 col-lg-12">
	                                    	    <div class="wizard bg--white">
	                                    	        <h5>Book Details</h5>
	                                    	        <section class="text-center">
	                                    	            <div class="pos-vertical-center">
	                                    	                <div class="row justify-content-center">
	                                    	                	<div class="col-md-9 text-left mb20">
	                                    	                		<label class="">Name:</label>
	                                    	                		 <input type="text" name="name" placeholder="Name" />
	                                    	                	</div>
	                                    	                	<div class="col-md-3 text-left mb20">
	                                    	                		<label class="">Quantity:</label>
	                                    	                		 <input type="number" name="quantity" placeholder="2" />
	                                    	                	</div>
	                                    	                	<div class="col-md-12 text-left mb20">
	                                    	                		
	                                    	                		<div class="input-select">
																		<select>
																			<option selected="" value="Default">Select drop option</option>
																			<option value="pick-up">Pick Up</option>
																			<option value="drop-off">Drop Off</option>
																			
																		</select>
																	</div>
	                                    	                	</div>
	                                    	                	<div class="col-md-6 text-left mb20">
	                                    	                		
	                                    	                		 <input type="text" name="street-name" placeholder="Street Name" />
	                                    	                	</div>
	                                    	                	<div class="col-md-6 text-left mb20">
	                                    	                		
	                                    	                		<div class="input-select">
																		<select>
																			<option selected="" value="Default">Select City</option>
																			<option value="101">Demsa</option><option value="1492">Fufore</option><option value="103">Ganye</option><option value="1493">Girei</option><option value="1494">Gombi</option><option value="1503">Guyuk</option><option value="1495">Hong</option><option value="1496">Jada</option><option value="5083">Jimeta</option><option value="1497">Lamurde</option><option value="1498">Madagali</option><option value="112">Maiha</option><option value="113">Mayo-Belwa</option><option value="114">Michika</option><option value="115">Mubi North</option><option value="1499">Mubi South</option><option value="117">Numan</option><option value="1500">Shelleng</option><option value="1501">Song</option><option value="120">Toungo</option><option value="121">Yola North</option><option value="1502">Yola South</option>
																			
																		</select>
																	</div>
	                                    	                	</div>
	                                    	                	<div class="col-md-6 text-left mb20">
	                                    	                		
	                                    	                		<div class="input-select">
																		<select>
																			<option selected="" value="Default">Select State</option>
																			<option value="2">Abia</option>
																			<option value="3">Adamawa </option>
																			<option value="4">Akwa Ibom</option>
																			<option value="5">Anambra</option>
																			<option value="6">Bauchi</option>
																			<option value="7">Bayelsa</option>
																			<option value="8">Benue</option>
																			<option value="9">Borno</option>
																			<option value="10">Cross River</option><option value="11">Delta</option><option value="12">Ebonyi</option><option value="13">Edo</option><option value="39">Ekiti</option><option value="15">Enugu</option><option value="37">Federal Capital Territory</option><option value="16">Gombe</option><option value="17">Imo</option><option value="18">Jigawa</option><option value="19">Kaduna</option><option value="20">Kano</option><option value="21">Katsina</option><option value="22">Kebbi</option><option value="23">Kogi</option><option value="52">Kwara</option><option value="1">Lagos</option><option value="25">Nasarawa</option><option value="26">Niger</option><option value="96">Nigeria</option><option value="27">Ogun</option><option value="28">Ondo</option><option value="29">Osun</option><option value="30">Oyo</option><option value="31">Plateau</option><option value="32">Rivers </option><option value="33">Sokoto</option><option value="34">Taraba</option><option value="35">Yobe</option><option value="36">Zamfara</option>
																			
																		</select>
																	</div>
	                                    	                	</div>
	                                    	                	<div class="col-md-6 text-left mb20">
	                                    	                		
	                                    	                		<div class="input-select">
																		<select>
																			
																			<option selected="" value="Default">Nigeria</option>
																			
																		</select>
																	</div>
	                                    	                	</div>
	                                    	                </div>
	                                    	            </div>
	                                    	        </section>
	                                    	        
	                                    	        <h5>Donate</h5>
	                                    	        <section class="text-center">
	                                    	            <div class="pos-vertical-center">
	                                    	                <img alt="pic" class="height-140" src="assets/img/art-5.png" />
	                                    	                <h4>Thank You for Donating!</h4>
	                                    	                <p class="type--fade type--fine-print">
	                                    	                  
	                                    	                </p>
	                                    	            </div>
	                                    	        </section>
	                                    	    </div>
	                                    	</div>
		                                    
		                                </form>
	                                <!--end of row-->
	                               
	                                                
	                            </div>
	                        </div>
	                    </div>
	                </div>
                </div>
            
                <div class="col-12 col-sm-4 col-lg-6 hvr-grow">
                    <a href="other-options">
                    	<div class="feature feature-1 boxed box-1 bg--violet-2 boxed--border mb-sm-24" data-aos="zoom-in" data-aos-delay="200" >
                    		<h5 class="type--uppercase color--primary mb16">Click here to</h5>
                    	    <h1 class="title font-600 equal color--white">Other <br> Options</h1>
                    	    

                    	    <svg xmlns="http://www.w3.org/2000/svg" class="pos-absolute circle-yellow" width="262" height="262" viewBox="0 0 262 262"><circle cx="131" cy="131" r="131" fill="#ecff86"  class="pos-absolute circle-yellow" /></svg>
                    	    <img src="assets/img/box-2.svg" class="anim-up-down pos-absolute anim-box-1">
                    	</div>
                    </a>
                </div>
                
            </div>
        </div>
    </section>


<section id="to-steps" class="bg--secondary-2 hidden-xs hidden-sm">
	<div class="container">
		<div class="row justify-content-between align-items-center">
			<div class="col-lg-5">

				<!-- Ellipse bg -->
				<svg class="ellipse" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="384" height="384" viewBox="0 0 384 384"><defs><linearGradient id="a" x1="0.5" x2="0" y2="0.975" gradientUnits="objectBoundingBox"><stop offset="0" stop-color="#75bafd"/><stop offset="1" stop-color="#4848c9"/></linearGradient></defs><circle cx="192" cy="192" r="192" opacity="0.747" fill="url(#a)"/></svg>

				<!-- Stars -->
				<svg class="star-blue left" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#323c8b"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#2b3271"/></g></svg>

				<svg class="star-blue right" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#323c8b"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#2b3271"/></g></svg>

				<svg class="star-yellow right" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#ffe206"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#ffe206"/></g></svg>

				<svg class="star-yellow left" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#ffe206"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#ffe206"/></g></svg>

				<!-- Clouds -->
				<svg class="cloud left" xmlns="http://www.w3.org/2000/svg" width="97.636" height="42.674" viewBox="0 0 97.636 42.674"><path d="M449.176,153.949c-4.062-3.9-5.743-7.9-11.338-9.154a10.542,10.542,0,0,1-3.341-1.015c-2.724-1.692-2.169-5.553-2.779-8.61-1.2-6.022-9.071-9.828-14.837-7.18-2.415,1.111-4.6,3.157-7.276,2.974-5.34-.363-4.7-6.95-8.35-10.707-3.447-3.548-10.468-4.011-14.948-1.762a22.922,22.922,0,0,0-7.867,6.389c-2.172,2.812-2.074,7.161-3.676,10.261-1.527,3-5.184,4.3-6.78,7.249a8.982,8.982,0,0,0-.681,7.978c2.28,5.474,8.972,9.423,15.039,9.423h81.953S453.7,158.288,449.176,153.949Z" transform="translate(-366.658 -117.122)" fill="#f3faff"/></svg>

				<svg class="cloud right" xmlns="http://www.w3.org/2000/svg" width="94.525" height="41.315" viewBox="0 0 94.525 41.315"><path d="M539,173.655c3.932-3.772,5.56-7.65,10.977-8.863a10.2,10.2,0,0,0,3.235-.983c2.637-1.639,2.1-5.376,2.691-8.334,1.166-5.83,8.781-9.516,14.364-6.951,2.338,1.074,4.458,3.054,7.044,2.879,5.169-.353,4.55-6.729,8.083-10.366,3.336-3.435,10.135-3.885,14.472-1.707a22.224,22.224,0,0,1,7.615,6.186c2.1,2.725,2.009,6.933,3.56,9.935,1.479,2.9,5.02,4.163,6.564,7.017a8.692,8.692,0,0,1,.659,7.724c-2.207,5.3-8.685,9.123-14.559,9.123H524.368S534.623,177.855,539,173.655Z" transform="translate(-524.368 -138.001)" fill="#f3faff"/></svg>

				<svg class="cloud left-big" xmlns="http://www.w3.org/2000/svg" width="159.475" height="41.315" viewBox="0 0 159.475 41.315"><path d="M659.151,173.655c-6.634-3.772-9.381-7.65-18.52-8.863a26.561,26.561,0,0,1-5.457-.983c-4.449-1.639-3.539-5.376-4.54-8.334-1.967-5.83-14.815-9.516-24.234-6.951-3.945,1.074-7.52,3.054-11.883,2.879-8.721-.353-7.676-6.729-13.638-10.366-5.628-3.435-17.1-3.885-24.416-1.707a40.155,40.155,0,0,0-12.848,6.186c-3.547,2.725-3.389,6.933-6.005,9.935-2.495,2.9-8.469,4.163-11.075,7.017-2.258,2.475-2.952,5.109-1.112,7.724,3.724,5.3,14.653,9.123,24.562,9.123H683.843S666.542,177.855,659.151,173.655Z" transform="translate(-524.368 -138.001)" fill="#f3faff"/></svg>

				<svg class="svg-box first" xmlns="http://www.w3.org/2000/svg" width="263.39" height="262.667" viewBox="0 0 263.39 262.667"><g style="isolation:isolate"><g transform="translate(125.771 72.614)"><path d="M482.754,260.024l-.312,110.035L345.134,450.077l.312-110.036Z" transform="translate(-345.134 -260.024)" fill="#ddb176"/></g><g transform="translate(0 80.018)"><path d="M449.523,333.915l-.312,110.036L323.441,371.336l.312-110.035Z" transform="translate(-323.441 -261.301)" fill="#c19564"/></g><g transform="translate(0.312)"><path d="M586.573,320.113,449.265,400.131,323.495,327.517,460.8,247.5Z" transform="translate(-323.495 -247.499)" fill="#fcd9a9"/></g></g><g transform="translate(51.676 29.962)"><path d="M470.538,252.667,332.354,332.448v25.686l14.566,8.409V340.649l138-79.677Z" transform="translate(-332.354 -252.667)" fill="#c58c67" opacity="0.7"/><path d="M335.224,333.982l137.949-79.647-1.794-1.036-137.95,79.647Z" transform="translate(-327.198 -249.638)" fill="#a47f65" opacity="0.2" style="mix-blend-mode:multiply;isolation:isolate"/><g transform="translate(89.736 57.664)"><path d="M410.481,295.719l-52.075,30.065v-33.1l52.075-30.066Z" transform="translate(-297.103 -262.614)" fill="#e4e4e4"/><path d="M398.793,282.764l-50.961,29.423v-4.464L398.793,278.3Zm-7.067,8.986L354.9,313.01v1.716l36.827-21.26Zm0,5.577L354.9,318.594v1.716l36.827-21.26Zm0,5.583L354.9,324.171v1.716l36.827-21.26Zm0,5.577L354.9,329.748v1.716l36.827-21.26Zm0,5.583L354.9,335.331v1.716l36.827-21.26Z" transform="translate(-347.832 -187.356)" fill="#363633" opacity="0.4"/><path d="M396.613,264.166l-36.827,21.26v1.716l36.827-21.26Zm0,5.577L359.786,291v1.716l36.827-21.26Zm0,5.577-36.827,21.26V298.3l36.827-21.266Zm0,5.583-36.827,21.26v1.716l36.827-21.26Zm0,5.577-36.827,21.26v1.716l36.827-21.26Z" transform="translate(-290.481 -255.166)" fill="#363633" opacity="0.4"/></g></g><path d="M332.354,266.428v25.685l14.567,8.41v-25.9Z" transform="translate(-280.679 -156.686)" fill="#c58c67" opacity="0.5" style="mix-blend-mode:multiply;isolation:isolate"/><g transform="translate(134.277 136.259)"><path d="M353.5,277.932c-1.02,2.36-3.281,4.273-5.055,4.273s-2.371-1.913-1.351-4.273,3.281-4.267,5.05-4.267S354.524,275.572,353.5,277.932Z" transform="translate(-346.601 -258.226)" fill="#4c4b44"/><path d="M355.809,276.6c-1.02,2.36-3.281,4.273-5.05,4.273s-2.377-1.913-1.356-4.273,3.287-4.267,5.055-4.267S356.829,274.246,355.809,276.6Z" transform="translate(-335.537 -264.617)" fill="#4c4b44"/><path d="M358.111,275.269c-1.02,2.36-3.281,4.267-5.05,4.267s-2.371-1.908-1.351-4.267S354.992,271,356.76,271,359.132,272.909,358.111,275.269Z" transform="translate(-324.47 -271.002)" fill="#4c4b44"/></g></svg>

				<img src="assets/img/box-2.svg" class="svg-box second">
			</div>
			<div class="col-lg-5">
				<h5 class="type--uppercase mb00 color--blue">Step 1</h5>
				<h1 class="font-600">Pack & Seal </h1>
				<p>Package your books and resources into cardboard boxes. Please read our guidelines as to what books are acceptable for donations here.</p>
				<a href="#" class="type--uppercase styled-link">See Books We Accept</a>
			</div>
		</div>
	</div>
</section>

<section id="to-step-2" class="bg--secondary-2 hidden-xs hidden-sm">
	<div class="container">
		<div class="row justify-content-between align-items-center">
			<div class="col-lg-5">
				<h5 class="type--uppercase mb00 color--blue">Step 2</h5>
				<h1 class="font-600">Choose Drop Off Point</h1>
				<p>Fill out the forms and choose your drop off point.  You can choose for pickup</p>
				
			</div>
			<div class="col-lg-5">

				<!-- Ellipse bg -->
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="384" height="384" viewBox="0 0 384 384"><defs><linearGradient id="c" x1="0.5" x2="0.083" y2="0.903" gradientUnits="objectBoundingBox"><stop offset="0" stop-color="#e3856f"/><stop offset="1" stop-color="#ffe762"/></linearGradient></defs><circle cx="192" cy="192" r="192" opacity="0.747" fill="url(#c)"/></svg>

				<!-- Stars -->
				<svg class="star-blue left top" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#323c8b"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#2b3271"/></g></svg>

				<svg class="star-blue right top" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#323c8b"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#2b3271"/></g></svg>

				<svg class="star-yellow right bottom" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#ffe206"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#ffe206"/></g></svg>

				<svg class="star-yellow left bottom" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#ffe206"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#ffe206"/></g></svg>

				<!-- Clouds -->
				<svg class="cloud left left-2" xmlns="http://www.w3.org/2000/svg" width="97.636" height="42.674" viewBox="0 0 97.636 42.674"><path d="M449.176,153.949c-4.062-3.9-5.743-7.9-11.338-9.154a10.542,10.542,0,0,1-3.341-1.015c-2.724-1.692-2.169-5.553-2.779-8.61-1.2-6.022-9.071-9.828-14.837-7.18-2.415,1.111-4.6,3.157-7.276,2.974-5.34-.363-4.7-6.95-8.35-10.707-3.447-3.548-10.468-4.011-14.948-1.762a22.922,22.922,0,0,0-7.867,6.389c-2.172,2.812-2.074,7.161-3.676,10.261-1.527,3-5.184,4.3-6.78,7.249a8.982,8.982,0,0,0-.681,7.978c2.28,5.474,8.972,9.423,15.039,9.423h81.953S453.7,158.288,449.176,153.949Z" transform="translate(-366.658 -117.122)" fill="#f3faff"/></svg>

				<svg class="cloud right" xmlns="http://www.w3.org/2000/svg" width="94.525" height="41.315" viewBox="0 0 94.525 41.315"><path d="M539,173.655c3.932-3.772,5.56-7.65,10.977-8.863a10.2,10.2,0,0,0,3.235-.983c2.637-1.639,2.1-5.376,2.691-8.334,1.166-5.83,8.781-9.516,14.364-6.951,2.338,1.074,4.458,3.054,7.044,2.879,5.169-.353,4.55-6.729,8.083-10.366,3.336-3.435,10.135-3.885,14.472-1.707a22.224,22.224,0,0,1,7.615,6.186c2.1,2.725,2.009,6.933,3.56,9.935,1.479,2.9,5.02,4.163,6.564,7.017a8.692,8.692,0,0,1,.659,7.724c-2.207,5.3-8.685,9.123-14.559,9.123H524.368S534.623,177.855,539,173.655Z" transform="translate(-524.368 -138.001)" fill="#f3faff"/></svg>

				<svg class="cloud line" xmlns="http://www.w3.org/2000/svg" width="145" height="9" viewBox="0 0 145 9"><g transform="translate(-1206 -1539)"><rect width="97" height="9" rx="4.5" transform="translate(1254 1539)" fill="#6379f5"/><rect width="40" height="9" rx="4.5" transform="translate(1206 1539)" fill="#6379f5"/></g></svg>

				<svg class="cloud line-bot" xmlns="http://www.w3.org/2000/svg" width="195" height="9" viewBox="0 0 195 9"><g transform="translate(-1173 -1556)"><rect width="97" height="9" rx="4.5" transform="translate(1173 1556)" fill="#677df5"/><rect width="40" height="9" rx="4.5" transform="translate(1278 1556)" fill="#677df5"/><rect width="40" height="9" rx="4.5" transform="translate(1328 1556)" fill="#677df5"/></g></svg>
				

				<img src="assets/img/box-3.svg" class="svg-box left-small">

				<!-- MAN -->
				<img src="assets/img/man-1.svg" class="man">
			</div>
			
		</div>
	</div>
</section>

<section id="to-step-3" class="bg--secondary-2 hidden-xs hidden-sm">
	<div class="container">
		<div class="row justify-content-between align-items-center">
			<div class="col-lg-5">

				<!-- Ellipse bg -->
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="384" height="384" viewBox="0 0 384 384"><defs><linearGradient id="d" x1="0.5" x2="0" y2="0.975" gradientUnits="objectBoundingBox"><stop offset="0" stop-color="#75bafd"/><stop offset="1" stop-color="#4848c9"/></linearGradient></defs><circle cx="192" cy="192" r="192" opacity="0.747" fill="url(#d)"/></svg>
				<!-- Clouds -->
				<svg class="cloud left " xmlns="http://www.w3.org/2000/svg" width="97.636" height="42.674" viewBox="0 0 97.636 42.674"><path d="M449.176,153.949c-4.062-3.9-5.743-7.9-11.338-9.154a10.542,10.542,0,0,1-3.341-1.015c-2.724-1.692-2.169-5.553-2.779-8.61-1.2-6.022-9.071-9.828-14.837-7.18-2.415,1.111-4.6,3.157-7.276,2.974-5.34-.363-4.7-6.95-8.35-10.707-3.447-3.548-10.468-4.011-14.948-1.762a22.922,22.922,0,0,0-7.867,6.389c-2.172,2.812-2.074,7.161-3.676,10.261-1.527,3-5.184,4.3-6.78,7.249a8.982,8.982,0,0,0-.681,7.978c2.28,5.474,8.972,9.423,15.039,9.423h81.953S453.7,158.288,449.176,153.949Z" transform="translate(-366.658 -117.122)" fill="#f3faff"/></svg>

				<svg class="cloud right" xmlns="http://www.w3.org/2000/svg" width="94.525" height="41.315" viewBox="0 0 94.525 41.315"><path d="M539,173.655c3.932-3.772,5.56-7.65,10.977-8.863a10.2,10.2,0,0,0,3.235-.983c2.637-1.639,2.1-5.376,2.691-8.334,1.166-5.83,8.781-9.516,14.364-6.951,2.338,1.074,4.458,3.054,7.044,2.879,5.169-.353,4.55-6.729,8.083-10.366,3.336-3.435,10.135-3.885,14.472-1.707a22.224,22.224,0,0,1,7.615,6.186c2.1,2.725,2.009,6.933,3.56,9.935,1.479,2.9,5.02,4.163,6.564,7.017a8.692,8.692,0,0,1,.659,7.724c-2.207,5.3-8.685,9.123-14.559,9.123H524.368S534.623,177.855,539,173.655Z" transform="translate(-524.368 -138.001)" fill="#f3faff"/></svg>

				<!-- Liquid -->
				<svg class="liquid" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="423.171" height="38.227" viewBox="0 0 423.171 38.227"><defs><linearGradient id="v" x1="0.5" y1="1" x2="0.5" gradientUnits="objectBoundingBox"><stop offset="0.005" stop-color="#fcf5f1"/><stop offset="0.477" stop-color="#fceeec"/><stop offset="1" stop-color="#fce1e3"/></linearGradient></defs><path d="M294.043,386.444H700.266c13.235,0-10.454,6.91-18.214,14.671s-7.308,10.988-26.621,16.861c-41.534,12.63-59.3-12.093-125.246-2.361-28.1,4.146-63.926,12.068-92.184,7.858-23.8-3.546-29.58-19.778-47.569-21.092-27.614-2.018-41.39-.035-66.585-1.266C302.261,400.06,259.516,386.444,294.043,386.444Z" transform="translate(-281.038 -386.444)" fill="url(#v)"/></svg>

				<svg class="path-step-3" xmlns="http://www.w3.org/2000/svg" width="350.184" height="191.027" viewBox="0 0 350.184 191.027"><g transform="translate(0 -19.835)"><path d="M520.963,322.046H647.895A35.546,35.546,0,0,0,683.442,286.5h0a35.547,35.547,0,0,0-35.547-35.546H376.9A18.855,18.855,0,0,1,358.046,232.1h0A18.855,18.855,0,0,1,376.9,213.243H604.6a16.7,16.7,0,0,0,16.7-16.7h0a16.7,16.7,0,0,0-16.7-16.7H396.234a9.186,9.186,0,0,1-9.185-9.185h0a9.186,9.186,0,0,1,9.185-9.185h60.583" transform="translate(-333.758 -111.684)" fill="none" stroke="#b3d3f4" stroke-miterlimit="10" stroke-width="1" stroke-dasharray="4"/><g transform="translate(114.838 19.835)"><path d="M461.268,135.064a19.334,19.334,0,0,0-16.724,32.9l-2.611,5.3,5.86-2.738a19.34,19.34,0,1,0,13.476-35.466Z" transform="translate(-438.707 -134.791)" fill="#f3faff"/><path d="M448.535,162.174l-2-2.4a2.176,2.176,0,0,1,.276-3.066l2.3-1.92a2.177,2.177,0,0,1,3.066.276s1.063,1.6,2.31,1.313c1.419-.324,4.505-2.576,6.165-4.322,1.17-1.229-.8-3.4-.8-3.4a2.178,2.178,0,0,1,.277-3.067l2.3-1.92a2.176,2.176,0,0,1,3.066.277l2,2.394C472.006,151.748,452.459,166.878,448.535,162.174Z" transform="translate(-437.809 -133.764)" fill="#b3d3f4"/></g><g transform="translate(269.666 23.723)"><path d="M615.023,188.588l-33.967-18.177L590,143.86l41.343,21.4Z" transform="translate(-576.084 -137.567)" fill="#cfe0f5"/><path d="M630.366,165.952l-41.343-21.4,29.47-6.293Z" transform="translate(-575.107 -138.254)" fill="#cfe0f5"/><rect width="37.269" height="31.481" transform="translate(19.008 6.282) rotate(27.373)" fill="#fff4e1"/><path d="M590.543,143.86l-13.916,26.879,27.138-1.788Z" transform="translate(-576.627 -137.567)" fill="#e6f1fc"/><path d="M628.922,162.927l-13.916,26.879-14.2-23.193Z" transform="translate(-573.663 -135.229)" fill="#e6f1fc"/><path d="M617.969,190.3l-41.343-21.4,31.439-9.96Z" transform="translate(-576.627 -135.719)" fill="#e6f1fc" style="mix-blend-mode:multiply;isolation:isolate"/><path d="M617.969,189.895l-41.343-21.4L606.1,162.2Z" transform="translate(-576.627 -135.319)" fill="#e6f1fc"/></g><g transform="translate(0 55.782)"><path d="M372.393,196l-27.678,6.93-8.3-19.016,33.33-8.829Z" transform="translate(-336.411 -165.798)" fill="#cfe0f5"/><path d="M369.74,176.1l-33.33,8.829,13.036-18.115Z" transform="translate(-336.411 -166.812)" fill="#cfe0f5"/><rect width="27.602" height="23.315" transform="matrix(0.967, -0.256, 0.256, 0.967, 2.788, 15.576)" fill="#fff4e1"/><path d="M336.411,182.949l5.74,21.669,14-14.484Z" transform="translate(-336.411 -164.834)" fill="#e6f1fc"/><path d="M367.585,175.085l5.74,21.669L353.993,191.1Z" transform="translate(-334.255 -165.798)" fill="#e6f1fc"/><path d="M374.853,195.728l-33.33,8.829,12.291-21.107Z" transform="translate(-335.784 -164.772)" fill="#e6f1fc" style="mix-blend-mode:multiply;isolation:isolate"/><path d="M374.853,195.4l-33.33,8.829,13.036-18.116Z" transform="translate(-335.784 -164.446)" fill="#e6f1fc"/></g></g></svg>


				<svg class="envelope" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="432.908" height="257.353" viewBox="0 0 432.908 257.353"><defs><linearGradient id="z" x1="0.5" y1="1" x2="0.5" gradientUnits="objectBoundingBox"><stop offset="0" stop-color="#466de5"/><stop offset="1" stop-color="#78bfff"/></linearGradient><linearGradient id="x" x1="1.294" y1="0.807" x2="-0.165" y2="0.279" xlink:href="#z"/><linearGradient id="e" x1="0.497" y1="0.987" x2="0.497" y2="0.033" xlink:href="#z"/><linearGradient id="f" x1="0.29" y1="1.082" x2="0.781" y2="0.221" xlink:href="#z"/><linearGradient id="g" x1="0.755" y1="1.136" x2="0.18" y2="0.178" gradientUnits="objectBoundingBox"><stop offset="0" stop-color="#b3d3f4"/><stop offset="1" stop-color="#d3dced"/></linearGradient><linearGradient id="h" x1="0.5" y1="1" x2="0.5" gradientUnits="objectBoundingBox"><stop offset="0" stop-color="#ffd5a7"/><stop offset="0.372" stop-color="#ffd7aa"/><stop offset="0.654" stop-color="#ffddb3"/><stop offset="0.907" stop-color="#ffe8c3"/><stop offset="1" stop-color="#fec"/></linearGradient><linearGradient id="i" x1="0.755" y1="1.235" x2="0.245" y2="-0.235" gradientUnits="objectBoundingBox"><stop offset="0" stop-color="#cde8ff"/><stop offset="1" stop-color="#eee7e9"/></linearGradient><linearGradient id="j" x1="0.75" y1="1.588" x2="0.25" y2="0.412" xlink:href="#i"/><linearGradient id="k" x1="-11.573" y1="1.069" x2="-11.037" y2="0.223" xlink:href="#z"/></defs><g transform="translate(-266.468 -176.834)"><g transform="translate(266.468 278.915)"><g transform="translate(339.423)"><path d="M568.932,401.793c-1.3-44.712,8.809-98.977,44.032-129.778,5.653-4.943,12.565-11.312,20.588-10.732,3.733.271,5.584,4.541,6.145,7.709,1,5.656.027,11.473-1.354,17.049-7.625,30.791-32.77,84.691-39.2,115.752" transform="translate(-568.823 -261.246)" fill="url(#z)"/><path d="M626.311,272.011C591.3,317.771,585,374.951,586.106,399.7" transform="translate(-566.719 -259.926)" fill="none" stroke="#92d0ff" stroke-miterlimit="10" stroke-width="1"/><g transform="translate(30.319 42.52)"><path d="M595.831,397.149c4.716-28.936,9.668-52.568,26.251-76.745,6.823-9.946,19.965-22.239,30.421-21.223,11.219,1.091,3.258,18.043-3.4,30.7A178.415,178.415,0,0,0,630.7,383.945c.926-5.671,4.861-21.82,16.489-29C650.6,352.842,659,351.611,659,358.6c0,12.28-11.937,27.028-14.923,38.549" transform="translate(-595.831 -299.122)" fill="url(#z)"/><path d="M647.062,307.129c-10.756,13.653-32.2,60.9-34.832,88.181" transform="translate(-593.821 -298.141)" fill="none" stroke="#92d0ff" stroke-miterlimit="10" stroke-width="1"/><path d="M648.084,353.017c-8.265,8.265-13.334,23.414-16.2,37.525" transform="translate(-591.411 -292.515)" fill="none" stroke="#92d0ff" stroke-miterlimit="10" stroke-width="1"/></g></g><g transform="translate(0 18.515)"><path d="M359.927,399.771c-6.82-56.413-1.865-81,4.558-96.622,2.993-7.282,7.293-14.494,13.38-19.632,24.357-20.555,23.617,19.409,22.473,31.523-2.607,27.6-9.524,57.1-10.151,84.731" transform="translate(-255.412 -277.739)" fill="url(#x)"/><path d="M346.476,399.643c-.773-17.747-8.695-35.061-18.783-49.383-10.147-14.4-24.1-25.894-33.239-41.023-4.895-8.106-12.639-20.851-1.187-27.5,12.7-7.37,31.353.357,41.566,8.783,11.107,9.164,18.406,22.146,24.5,34.967,10.762,22.645,12.922,49.082,13.091,74.154" transform="translate(-263.912 -277.611)" fill="url(#z)"/><path d="M351.452,394.62c.051-18.064-9.153-37.133-23.687-51.243-9.495-9.219-20.854-17.858-33.808-21.66-6.724-1.975-25.52-5.1-27.4,6.075-.714,4.234,3.107,12.194,12.263,19.444,12.729,10.079,47.894,25.751,52.136,47.384" transform="translate(-266.468 -272.588)" fill="url(#e)"/><path d="M274.265,327.392c24.361,6.624,58.32,28.86,64.775,65.856" transform="translate(-265.512 -271.652)" fill="none" stroke="#92d0ff" stroke-miterlimit="10" stroke-width="1"/><path d="M298.109,287.675c24.414,12.875,54.19,53.083,57.521,110.442" transform="translate(-262.589 -276.521)" fill="none" stroke="#92d0ff" stroke-miterlimit="10" stroke-width="1"/><path d="M385.529,282.592c-13.723,33.328-16.113,80.114-15.316,116.148" transform="translate(-253.766 -277.144)" fill="none" stroke="#92d0ff" stroke-miterlimit="10" stroke-width="1"/></g><g transform="translate(25.993 99.229)"><path d="M331.395,390.956c-2.18-9.744-4.1-19.9-9.9-28.3-3.8-5.491-11.545-13.141-18.44-13.021-5.126.089-6.291,5.019-5.695,9.458,1.564,11.635,8.015,15.185,10.52,27.765-1.249-4.765-8.939-21.266-15.69-14.957-7.426,6.939,3.647,19.05,3.647,19.05" transform="translate(-289.623 -349.639)" fill="url(#f)"/><path d="M294.487,372.21c3.532,4.045,8.066,9.815,9.892,15.979" transform="translate(-289.027 -346.872)" fill="none" stroke="#92d0ff" stroke-miterlimit="10" stroke-width="1"/><path d="M303.994,356c9.352,9.353,15.884,29.09,17.577,34.175" transform="translate(-287.861 -348.859)" fill="none" stroke="#92d0ff" stroke-miterlimit="10" stroke-width="1"/></g></g><g transform="translate(394.464 176.834)"><path d="M500.824,170.313,380.485,275.684v144.9H621.163v-144.9Z" transform="translate(-380.485 -170.313)" fill="url(#g)"/><rect width="216.348" height="198.247" transform="translate(12.165 37.067)" fill="url(#h)"/><path d="M500.824,356.165,380.485,264.177V394.119H621.163V264.177Z" transform="translate(-380.485 -158.805)" fill="#c9d3f4" opacity="0.55" style="mix-blend-mode:multiply;isolation:isolate"/><path d="M500.824,371.142,380.485,264.177v144.9H621.163v-144.9Z" transform="translate(-380.485 -158.805)" fill="url(#i)"/><path d="M500.824,292.189,380.485,405.638H621.163Z" transform="translate(-380.485 -155.371)" fill="#c9d3f4" opacity="0.35" style="mix-blend-mode:multiply;isolation:isolate"/><path d="M500.824,302.039,380.485,404.431H621.163Z" transform="translate(-380.485 -154.164)" fill="url(#j)"/></g><g transform="translate(592.581 380.718)"><path d="M557.9,405.346c-2.436-12.693.108-25.728,5.18-36.58,3.317-7.1,10.961-16.989,19.486-16.833,6.337.116,9,6.49,9.364,12.227.961,15.039-6.106,19.631-6.069,35.893.356-6.159,6.313-26.159,15.618-19.336,9.112,6.682.236,24.629.236,24.629" transform="translate(-556.967 -351.931)" fill="url(#k)"/><path d="M595.257,381.11c-3.12,5.23-6.982,12.69-7.585,20.659" transform="translate(-553.202 -348.354)" fill="none" stroke="#92d0ff" stroke-miterlimit="10" stroke-width="1"/><path d="M580.62,360.156c-8.6,12.091-11.327,37.607-12.041,44.182" transform="translate(-555.543 -350.923)" fill="none" stroke="#92d0ff" stroke-miterlimit="10" stroke-width="1"/></g></g></svg>

				
			</div>
			<div class="col-lg-5">
				<h5 class="type--uppercase mb00 color--blue">Step 3</h5>
				<h1 class="font-600">Happily <br> Received!</h1>
				<p>When your books arrive in Nigeria, they are distributed to students and teachers in need. Students or teachers who receive any of your labelled items would send us a photo for you. We also publish detailed impact reports on our website. Keep Updated!</p>
				<div class="modal-instance">
					<a href="#" class="btn btn--blue btn--lg rounded modal-trigger">
						<span class="btn__text type--uppercase">Start Donating Now</span>
					</a>
					<div class="modal-container">
                        <div class="modal-content md">
                            <div class="boxed boxed--lg text-center">
                               
	                                <form class="justify-content-center">
	                                	<div class="col-md-12 col-lg-12">
	                                		<h2 class="mb8">Donate a Book</h2>
	                                	</div>
	                                    
                                    	<div class="col-md-12 col-lg-12">
                                    	    <div class="wizard bg--white">
                                    	        <h5>Book Details</h5>
                                    	        <section class="text-center">
                                    	            <div class="pos-vertical-center">
                                    	                <div class="row justify-content-center">
                                    	                	<div class="col-md-9 text-left mb20">
                                    	                		<label class="">Name:</label>
                                    	                		 <input type="text" name="name" placeholder="Name" />
                                    	                	</div>
                                    	                	<div class="col-md-3 text-left mb20">
                                    	                		<label class="">Quantity:</label>
                                    	                		 <input type="number" name="quantity" placeholder="2" />
                                    	                	</div>
                                    	                	<div class="col-md-6 text-left mb20">
	                                    	                		
	                                    	                		 <input type="text" name="street-name" placeholder="Street Name" />
	                                    	                	</div>
	                                    	                	<div class="col-md-6 text-left mb20">
	                                    	                		
	                                    	                		<div class="input-select">
																		<select>
																			<option selected="" value="Default">Select City</option>
																			<option value="101">Demsa</option><option value="1492">Fufore</option><option value="103">Ganye</option><option value="1493">Girei</option><option value="1494">Gombi</option><option value="1503">Guyuk</option><option value="1495">Hong</option><option value="1496">Jada</option><option value="5083">Jimeta</option><option value="1497">Lamurde</option><option value="1498">Madagali</option><option value="112">Maiha</option><option value="113">Mayo-Belwa</option><option value="114">Michika</option><option value="115">Mubi North</option><option value="1499">Mubi South</option><option value="117">Numan</option><option value="1500">Shelleng</option><option value="1501">Song</option><option value="120">Toungo</option><option value="121">Yola North</option><option value="1502">Yola South</option>
																			
																		</select>
																	</div>
	                                    	                	</div>
	                                    	                	<div class="col-md-6 text-left mb20">
	                                    	                		
	                                    	                		<div class="input-select">
																		<select>
																			<option selected="" value="Default">Select State</option>
																			<option value="2">Abia</option>
																			<option value="3">Adamawa </option>
																			<option value="4">Akwa Ibom</option>
																			<option value="5">Anambra</option>
																			<option value="6">Bauchi</option>
																			<option value="7">Bayelsa</option>
																			<option value="8">Benue</option>
																			<option value="9">Borno</option>
																			<option value="10">Cross River</option><option value="11">Delta</option><option value="12">Ebonyi</option><option value="13">Edo</option><option value="39">Ekiti</option><option value="15">Enugu</option><option value="37">Federal Capital Territory</option><option value="16">Gombe</option><option value="17">Imo</option><option value="18">Jigawa</option><option value="19">Kaduna</option><option value="20">Kano</option><option value="21">Katsina</option><option value="22">Kebbi</option><option value="23">Kogi</option><option value="52">Kwara</option><option value="1">Lagos</option><option value="25">Nasarawa</option><option value="26">Niger</option><option value="96">Nigeria</option><option value="27">Ogun</option><option value="28">Ondo</option><option value="29">Osun</option><option value="30">Oyo</option><option value="31">Plateau</option><option value="32">Rivers </option><option value="33">Sokoto</option><option value="34">Taraba</option><option value="35">Yobe</option><option value="36">Zamfara</option>
																			
																		</select>
																	</div>
	                                    	                	</div>
	                                    	                	<div class="col-md-6 text-left mb20">
	                                    	                		
	                                    	                		<div class="input-select">
																		<select>
																			
																			<option selected="" value="Default">Nigeria</option>
																			
																		</select>
																	</div>
	                                    	                	</div>
                                    	                </div>
                                    	            </div>
                                    	        </section>
                                    	        
                                    	        <h5>Donate</h5>
                                    	        <section class="text-center">
                                    	            <div class="pos-vertical-center">
                                    	                <img alt="pic" class="height-140" src="assets/img/art-5.png" />
                                    	                <h4>Thank You for Donating!</h4>
                                    	                <p class="type--fade type--fine-print">
                                    	                  
                                    	                </p>
                                    	            </div>
                                    	        </section>
                                    	    </div>
                                    	</div>
	                                    
	                                </form>
                                <!--end of row-->
                               
                                                
                            </div>
                        </div>
                    </div>
				</div>
			</div>
			
		</div>
	</div>
</section>

<section id="to-step" class="bg--secondary-2 hidden-lg hidden-md visible-xs visible-sm text-center">
	<div class="container">
		<div class="row">
			<div class="col-12">
                <div class="slider" data-arrows="true" data-paging="true"data-autoplay="false">
                    <ul class="slides">
                        <li>
                        	<div class="mb80">
                        		<h5 class="type--uppercase mb00 color--blue">Step 1</h5>
                        		<h1 class="font-600">Pack & Seal </h1>
                        		<p>Package your books and resources into cardboard boxes. Please read our guidelines as to what books are acceptable for donations here.</p>
                        		<a href="#" class="type--uppercase styled-link">See Books We Accept</a>
                        	</div>
                            <div class="pos-relative">
                            	<!-- Ellipse bg -->
								<img src="assets/img/ellipse-1.svg">

								<!-- Stars -->
								<svg class="star-blue left" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#323c8b"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#2b3271"/></g></svg>

								<svg class="star-blue right" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#323c8b"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#2b3271"/></g></svg>

								<svg class="star-yellow right" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#ffe206"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#ffe206"/></g></svg>

								<svg class="star-yellow left" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#ffe206"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#ffe206"/></g></svg>

								<!-- Clouds -->
								<svg class="cloud left" xmlns="http://www.w3.org/2000/svg" width="97.636" height="42.674" viewBox="0 0 97.636 42.674"><path d="M449.176,153.949c-4.062-3.9-5.743-7.9-11.338-9.154a10.542,10.542,0,0,1-3.341-1.015c-2.724-1.692-2.169-5.553-2.779-8.61-1.2-6.022-9.071-9.828-14.837-7.18-2.415,1.111-4.6,3.157-7.276,2.974-5.34-.363-4.7-6.95-8.35-10.707-3.447-3.548-10.468-4.011-14.948-1.762a22.922,22.922,0,0,0-7.867,6.389c-2.172,2.812-2.074,7.161-3.676,10.261-1.527,3-5.184,4.3-6.78,7.249a8.982,8.982,0,0,0-.681,7.978c2.28,5.474,8.972,9.423,15.039,9.423h81.953S453.7,158.288,449.176,153.949Z" transform="translate(-366.658 -117.122)" fill="#f3faff"/></svg>

								<svg class="cloud right" xmlns="http://www.w3.org/2000/svg" width="94.525" height="41.315" viewBox="0 0 94.525 41.315"><path d="M539,173.655c3.932-3.772,5.56-7.65,10.977-8.863a10.2,10.2,0,0,0,3.235-.983c2.637-1.639,2.1-5.376,2.691-8.334,1.166-5.83,8.781-9.516,14.364-6.951,2.338,1.074,4.458,3.054,7.044,2.879,5.169-.353,4.55-6.729,8.083-10.366,3.336-3.435,10.135-3.885,14.472-1.707a22.224,22.224,0,0,1,7.615,6.186c2.1,2.725,2.009,6.933,3.56,9.935,1.479,2.9,5.02,4.163,6.564,7.017a8.692,8.692,0,0,1,.659,7.724c-2.207,5.3-8.685,9.123-14.559,9.123H524.368S534.623,177.855,539,173.655Z" transform="translate(-524.368 -138.001)" fill="#f3faff"/></svg>

								<svg class="cloud left-big" xmlns="http://www.w3.org/2000/svg" width="159.475" height="41.315" viewBox="0 0 159.475 41.315"><path d="M659.151,173.655c-6.634-3.772-9.381-7.65-18.52-8.863a26.561,26.561,0,0,1-5.457-.983c-4.449-1.639-3.539-5.376-4.54-8.334-1.967-5.83-14.815-9.516-24.234-6.951-3.945,1.074-7.52,3.054-11.883,2.879-8.721-.353-7.676-6.729-13.638-10.366-5.628-3.435-17.1-3.885-24.416-1.707a40.155,40.155,0,0,0-12.848,6.186c-3.547,2.725-3.389,6.933-6.005,9.935-2.495,2.9-8.469,4.163-11.075,7.017-2.258,2.475-2.952,5.109-1.112,7.724,3.724,5.3,14.653,9.123,24.562,9.123H683.843S666.542,177.855,659.151,173.655Z" transform="translate(-524.368 -138.001)" fill="#f3faff"/></svg>

								<svg class="svg-box first" xmlns="http://www.w3.org/2000/svg" width="263.39" height="262.667" viewBox="0 0 263.39 262.667"><g style="isolation:isolate"><g transform="translate(125.771 72.614)"><path d="M482.754,260.024l-.312,110.035L345.134,450.077l.312-110.036Z" transform="translate(-345.134 -260.024)" fill="#ddb176"/></g><g transform="translate(0 80.018)"><path d="M449.523,333.915l-.312,110.036L323.441,371.336l.312-110.035Z" transform="translate(-323.441 -261.301)" fill="#c19564"/></g><g transform="translate(0.312)"><path d="M586.573,320.113,449.265,400.131,323.495,327.517,460.8,247.5Z" transform="translate(-323.495 -247.499)" fill="#fcd9a9"/></g></g><g transform="translate(51.676 29.962)"><path d="M470.538,252.667,332.354,332.448v25.686l14.566,8.409V340.649l138-79.677Z" transform="translate(-332.354 -252.667)" fill="#c58c67" opacity="0.7"/><path d="M335.224,333.982l137.949-79.647-1.794-1.036-137.95,79.647Z" transform="translate(-327.198 -249.638)" fill="#a47f65" opacity="0.2" style="mix-blend-mode:multiply;isolation:isolate"/><g transform="translate(89.736 57.664)"><path d="M410.481,295.719l-52.075,30.065v-33.1l52.075-30.066Z" transform="translate(-297.103 -262.614)" fill="#e4e4e4"/><path d="M398.793,282.764l-50.961,29.423v-4.464L398.793,278.3Zm-7.067,8.986L354.9,313.01v1.716l36.827-21.26Zm0,5.577L354.9,318.594v1.716l36.827-21.26Zm0,5.583L354.9,324.171v1.716l36.827-21.26Zm0,5.577L354.9,329.748v1.716l36.827-21.26Zm0,5.583L354.9,335.331v1.716l36.827-21.26Z" transform="translate(-347.832 -187.356)" fill="#363633" opacity="0.4"/><path d="M396.613,264.166l-36.827,21.26v1.716l36.827-21.26Zm0,5.577L359.786,291v1.716l36.827-21.26Zm0,5.577-36.827,21.26V298.3l36.827-21.266Zm0,5.583-36.827,21.26v1.716l36.827-21.26Zm0,5.577-36.827,21.26v1.716l36.827-21.26Z" transform="translate(-290.481 -255.166)" fill="#363633" opacity="0.4"/></g></g><path d="M332.354,266.428v25.685l14.567,8.41v-25.9Z" transform="translate(-280.679 -156.686)" fill="#c58c67" opacity="0.5" style="mix-blend-mode:multiply;isolation:isolate"/><g transform="translate(134.277 136.259)"><path d="M353.5,277.932c-1.02,2.36-3.281,4.273-5.055,4.273s-2.371-1.913-1.351-4.273,3.281-4.267,5.05-4.267S354.524,275.572,353.5,277.932Z" transform="translate(-346.601 -258.226)" fill="#4c4b44"/><path d="M355.809,276.6c-1.02,2.36-3.281,4.273-5.05,4.273s-2.377-1.913-1.356-4.273,3.287-4.267,5.055-4.267S356.829,274.246,355.809,276.6Z" transform="translate(-335.537 -264.617)" fill="#4c4b44"/><path d="M358.111,275.269c-1.02,2.36-3.281,4.267-5.05,4.267s-2.371-1.908-1.351-4.267S354.992,271,356.76,271,359.132,272.909,358.111,275.269Z" transform="translate(-324.47 -271.002)" fill="#4c4b44"/></g></svg>

								<img src="assets/img/box-2.svg" class="svg-box second">
                            </div>

							
                        </li>
                        <li>
                        	<div class="mb80">
                        		<h5 class="type--uppercase mb00 color--blue">Step 2</h5>
                        		<h1 class="font-600">Choose Drop<br> Off Point </h1>
                        		<p>Fill out the forms and choose your drop off point. You can choose for pickup.</p>
                        	
                        	</div>
                            <div class="pos-relative">
                            	<!-- Ellipse bg -->
								<img src="assets/img/ellipse-2.svg">

								<!-- Stars -->
								<svg class="star-blue left top" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#323c8b"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#2b3271"/></g></svg>

								<svg class="star-blue right top" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#323c8b"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#2b3271"/></g></svg>

								<svg class="star-yellow right bottom" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#ffe206"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#ffe206"/></g></svg>

								<svg class="star-yellow left bottom" xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27"><g transform="translate(-218 -860)"><ellipse cx="1.5" cy="13.5" rx="1.5" ry="13.5" transform="translate(233 860)" fill="#ffe206"/><ellipse cx="16.5" cy="1.5" rx="16.5" ry="1.5" transform="translate(218 872)" fill="#ffe206"/></g></svg>

								<!-- Clouds -->
								<svg class="cloud left left-2" xmlns="http://www.w3.org/2000/svg" width="97.636" height="42.674" viewBox="0 0 97.636 42.674"><path d="M449.176,153.949c-4.062-3.9-5.743-7.9-11.338-9.154a10.542,10.542,0,0,1-3.341-1.015c-2.724-1.692-2.169-5.553-2.779-8.61-1.2-6.022-9.071-9.828-14.837-7.18-2.415,1.111-4.6,3.157-7.276,2.974-5.34-.363-4.7-6.95-8.35-10.707-3.447-3.548-10.468-4.011-14.948-1.762a22.922,22.922,0,0,0-7.867,6.389c-2.172,2.812-2.074,7.161-3.676,10.261-1.527,3-5.184,4.3-6.78,7.249a8.982,8.982,0,0,0-.681,7.978c2.28,5.474,8.972,9.423,15.039,9.423h81.953S453.7,158.288,449.176,153.949Z" transform="translate(-366.658 -117.122)" fill="#f3faff"/></svg>

								<svg class="cloud right" xmlns="http://www.w3.org/2000/svg" width="94.525" height="41.315" viewBox="0 0 94.525 41.315"><path d="M539,173.655c3.932-3.772,5.56-7.65,10.977-8.863a10.2,10.2,0,0,0,3.235-.983c2.637-1.639,2.1-5.376,2.691-8.334,1.166-5.83,8.781-9.516,14.364-6.951,2.338,1.074,4.458,3.054,7.044,2.879,5.169-.353,4.55-6.729,8.083-10.366,3.336-3.435,10.135-3.885,14.472-1.707a22.224,22.224,0,0,1,7.615,6.186c2.1,2.725,2.009,6.933,3.56,9.935,1.479,2.9,5.02,4.163,6.564,7.017a8.692,8.692,0,0,1,.659,7.724c-2.207,5.3-8.685,9.123-14.559,9.123H524.368S534.623,177.855,539,173.655Z" transform="translate(-524.368 -138.001)" fill="#f3faff"/></svg>

								<svg class="cloud line" xmlns="http://www.w3.org/2000/svg" width="145" height="9" viewBox="0 0 145 9"><g transform="translate(-1206 -1539)"><rect width="97" height="9" rx="4.5" transform="translate(1254 1539)" fill="#6379f5"/><rect width="40" height="9" rx="4.5" transform="translate(1206 1539)" fill="#6379f5"/></g></svg>

								<svg class="cloud line-bot" xmlns="http://www.w3.org/2000/svg" width="195" height="9" viewBox="0 0 195 9"><g transform="translate(-1173 -1556)"><rect width="97" height="9" rx="4.5" transform="translate(1173 1556)" fill="#677df5"/><rect width="40" height="9" rx="4.5" transform="translate(1278 1556)" fill="#677df5"/><rect width="40" height="9" rx="4.5" transform="translate(1328 1556)" fill="#677df5"/></g></svg>
								

								<img src="assets/img/box-3.svg" class="svg-box left-small">

								<!-- MAN -->
								<img src="assets/img/man-1.svg" class="man">
                            </div>

							
                        </li>
                        <li>
                        	<div class="mb80">
                        		<h5 class="type--uppercase mb00 color--blue">Step 3</h5>
                        		<h1 class="font-600">Happily <br>Received! </h1>
                        		<p>When your books arrive in Nigeria, they are distributed to students and teachers in need. Students or teachers who receive any of your labelled items would send us a photo for you. We also publish detailed impact reports on our website. Keep Updated!</p>
                        		<div class="modal-instance">
									<a href="#" class="btn btn--blue btn--lg rounded modal-trigger">
										<span class="btn__text type--uppercase">Start Donating Now</span>
									</a>
									<div class="modal-container">
				                        <div class="modal-content md">
				                            <div class="boxed boxed--lg text-center">
				                               
					                                <form class="justify-content-center">
					                                	<div class="col-md-12 col-lg-12">
					                                		<h2 class="mb8">Donate a Book</h2>
					                                	</div>
					                                    
				                                    	<div class="col-md-12 col-lg-12">
				                                    	    <div class="wizard bg--white">
				                                    	        <h5>Book Details</h5>
				                                    	        <section class="text-center">
				                                    	            <div class="pos-vertical-center">
				                                    	                <div class="row justify-content-center">
				                                    	                	<div class="col-md-8 text-left mb20">
				                                    	                		<label class="">Name:</label>
				                                    	                		 <input type="text" name="name" placeholder="Name" />
				                                    	                	</div>
				                                    	                	<div class="col-md-3 text-left mb20">
				                                    	                		<label class="">Quantity:</label>
				                                    	                		 <input type="number" name="quantity" placeholder="2" />
				                                    	                	</div>
				                                    	                	<div class="col-md-11 text-left">
				                                    	                		
				                                    	                		<div class="input-select">
																					<select>
																						<option selected="" value="Default">Select An Option</option>
																						<option value="pick-up">Pick Up</option>
																						<option value="drop-off">Drop Off</option>
																						
																					</select>
																				</div>
				                                    	                	</div>

				                                    	                </div>
				                                    	            </div>
				                                    	        </section>
				                                    	        
				                                    	        <h5>Donate</h5>
				                                    	        <section class="text-center">
				                                    	            <div class="pos-vertical-center">
				                                    	                <img alt="pic" class="height-140" src="assets/img/art-5.png" />
				                                    	                <h4>Thank You for Donating!</h4>
				                                    	                <p class="type--fade type--fine-print">
				                                    	                  
				                                    	                </p>
				                                    	            </div>
				                                    	        </section>
				                                    	    </div>
				                                    	</div>
					                                    
					                                </form>
				                                <!--end of row-->
				                               
				                                                
				                            </div>
				                        </div>
				                    </div>
								</div>
                        
                        	</div>
                            <div class="pos-relative text-center">
                            	<!-- Ellipse bg -->
								<img src="assets/img/ellipse-1.svg">
            					<!-- Clouds -->
            					<svg class="cloud left " xmlns="http://www.w3.org/2000/svg" width="97.636" height="42.674" viewBox="0 0 97.636 42.674"><path d="M449.176,153.949c-4.062-3.9-5.743-7.9-11.338-9.154a10.542,10.542,0,0,1-3.341-1.015c-2.724-1.692-2.169-5.553-2.779-8.61-1.2-6.022-9.071-9.828-14.837-7.18-2.415,1.111-4.6,3.157-7.276,2.974-5.34-.363-4.7-6.95-8.35-10.707-3.447-3.548-10.468-4.011-14.948-1.762a22.922,22.922,0,0,0-7.867,6.389c-2.172,2.812-2.074,7.161-3.676,10.261-1.527,3-5.184,4.3-6.78,7.249a8.982,8.982,0,0,0-.681,7.978c2.28,5.474,8.972,9.423,15.039,9.423h81.953S453.7,158.288,449.176,153.949Z" transform="translate(-366.658 -117.122)" fill="#f3faff"/></svg>

            					<svg class="cloud right" xmlns="http://www.w3.org/2000/svg" width="94.525" height="41.315" viewBox="0 0 94.525 41.315"><path d="M539,173.655c3.932-3.772,5.56-7.65,10.977-8.863a10.2,10.2,0,0,0,3.235-.983c2.637-1.639,2.1-5.376,2.691-8.334,1.166-5.83,8.781-9.516,14.364-6.951,2.338,1.074,4.458,3.054,7.044,2.879,5.169-.353,4.55-6.729,8.083-10.366,3.336-3.435,10.135-3.885,14.472-1.707a22.224,22.224,0,0,1,7.615,6.186c2.1,2.725,2.009,6.933,3.56,9.935,1.479,2.9,5.02,4.163,6.564,7.017a8.692,8.692,0,0,1,.659,7.724c-2.207,5.3-8.685,9.123-14.559,9.123H524.368S534.623,177.855,539,173.655Z" transform="translate(-524.368 -138.001)" fill="#f3faff"/></svg>

            					<!-- Liquid -->
            					<svg class="liquid" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="423.171" height="38.227" viewBox="0 0 423.171 38.227"><defs><linearGradient id="v" x1="0.5" y1="1" x2="0.5" gradientUnits="objectBoundingBox"><stop offset="0.005" stop-color="#fcf5f1"/><stop offset="0.477" stop-color="#fceeec"/><stop offset="1" stop-color="#fce1e3"/></linearGradient></defs><path d="M294.043,386.444H700.266c13.235,0-10.454,6.91-18.214,14.671s-7.308,10.988-26.621,16.861c-41.534,12.63-59.3-12.093-125.246-2.361-28.1,4.146-63.926,12.068-92.184,7.858-23.8-3.546-29.58-19.778-47.569-21.092-27.614-2.018-41.39-.035-66.585-1.266C302.261,400.06,259.516,386.444,294.043,386.444Z" transform="translate(-281.038 -386.444)" fill="url(#v)"/></svg>

            					<svg class="path-step-3" xmlns="http://www.w3.org/2000/svg" width="350.184" height="191.027" viewBox="0 0 350.184 191.027"><g transform="translate(0 -19.835)"><path d="M520.963,322.046H647.895A35.546,35.546,0,0,0,683.442,286.5h0a35.547,35.547,0,0,0-35.547-35.546H376.9A18.855,18.855,0,0,1,358.046,232.1h0A18.855,18.855,0,0,1,376.9,213.243H604.6a16.7,16.7,0,0,0,16.7-16.7h0a16.7,16.7,0,0,0-16.7-16.7H396.234a9.186,9.186,0,0,1-9.185-9.185h0a9.186,9.186,0,0,1,9.185-9.185h60.583" transform="translate(-333.758 -111.684)" fill="none" stroke="#b3d3f4" stroke-miterlimit="10" stroke-width="1" stroke-dasharray="4"/><g transform="translate(114.838 19.835)"><path d="M461.268,135.064a19.334,19.334,0,0,0-16.724,32.9l-2.611,5.3,5.86-2.738a19.34,19.34,0,1,0,13.476-35.466Z" transform="translate(-438.707 -134.791)" fill="#f3faff"/><path d="M448.535,162.174l-2-2.4a2.176,2.176,0,0,1,.276-3.066l2.3-1.92a2.177,2.177,0,0,1,3.066.276s1.063,1.6,2.31,1.313c1.419-.324,4.505-2.576,6.165-4.322,1.17-1.229-.8-3.4-.8-3.4a2.178,2.178,0,0,1,.277-3.067l2.3-1.92a2.176,2.176,0,0,1,3.066.277l2,2.394C472.006,151.748,452.459,166.878,448.535,162.174Z" transform="translate(-437.809 -133.764)" fill="#b3d3f4"/></g><g transform="translate(269.666 23.723)"><path d="M615.023,188.588l-33.967-18.177L590,143.86l41.343,21.4Z" transform="translate(-576.084 -137.567)" fill="#cfe0f5"/><path d="M630.366,165.952l-41.343-21.4,29.47-6.293Z" transform="translate(-575.107 -138.254)" fill="#cfe0f5"/><rect width="37.269" height="31.481" transform="translate(19.008 6.282) rotate(27.373)" fill="#fff4e1"/><path d="M590.543,143.86l-13.916,26.879,27.138-1.788Z" transform="translate(-576.627 -137.567)" fill="#e6f1fc"/><path d="M628.922,162.927l-13.916,26.879-14.2-23.193Z" transform="translate(-573.663 -135.229)" fill="#e6f1fc"/><path d="M617.969,190.3l-41.343-21.4,31.439-9.96Z" transform="translate(-576.627 -135.719)" fill="#e6f1fc" style="mix-blend-mode:multiply;isolation:isolate"/><path d="M617.969,189.895l-41.343-21.4L606.1,162.2Z" transform="translate(-576.627 -135.319)" fill="#e6f1fc"/></g><g transform="translate(0 55.782)"><path d="M372.393,196l-27.678,6.93-8.3-19.016,33.33-8.829Z" transform="translate(-336.411 -165.798)" fill="#cfe0f5"/><path d="M369.74,176.1l-33.33,8.829,13.036-18.115Z" transform="translate(-336.411 -166.812)" fill="#cfe0f5"/><rect width="27.602" height="23.315" transform="matrix(0.967, -0.256, 0.256, 0.967, 2.788, 15.576)" fill="#fff4e1"/><path d="M336.411,182.949l5.74,21.669,14-14.484Z" transform="translate(-336.411 -164.834)" fill="#e6f1fc"/><path d="M367.585,175.085l5.74,21.669L353.993,191.1Z" transform="translate(-334.255 -165.798)" fill="#e6f1fc"/><path d="M374.853,195.728l-33.33,8.829,12.291-21.107Z" transform="translate(-335.784 -164.772)" fill="#e6f1fc" style="mix-blend-mode:multiply;isolation:isolate"/><path d="M374.853,195.4l-33.33,8.829,13.036-18.116Z" transform="translate(-335.784 -164.446)" fill="#e6f1fc"/></g></g></svg>


            					<img src="assets/img/envelope.svg" class="envelope">

                            </div>

							
                        </li>
                    </ul>
                </div>
            </div>
		</div>
	</div>
</section>



<?php include 'elements/footer.php'; ?>