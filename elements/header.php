<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Book Angel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link href="assets/css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
        <link href="assets/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <!-- <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="assets/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
         <link href="assets/css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/theme-yeller.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/padding.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/margin.min.css" rel="stylesheet" type="text/css" media="all" />
         <link href="assets/css/hover.min.css" rel="stylesheet" type="text/css" media="all" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

        <link href="assets/css/global.css" rel="stylesheet" type="text/css" media="all" />

        <?php if ($page == 'Home') { ?>
            <link href="assets/css/home.css" rel="stylesheet" type="text/css" media="all" />
        <?php } ?>
        <?php if ($page == 'Donate') { ?>
            <link href="assets/css/donate.css" rel="stylesheet" type="text/css" media="all" />
        <?php } ?>
        <?php if ($page == 'Books') { ?>
            <link href="assets/css/books.css" rel="stylesheet" type="text/css" media="all" />
        <?php } ?>

        <link href="assets/css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,900|Material+Icons" rel="stylesheet">

        <!-- <link rel="stylesheet" href="assets/css/style.6d80b553.css"> -->
        
    </head>
    <body data-smooth-scroll-offset="77">
        <?php include 'menu.php'; ?>
        <div class="main-container">